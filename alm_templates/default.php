<?php

global $post;

if ( ! is_a( $post, 'WP_Post' ) || empty( $post ) ) {
  return;
}

switch ( $post->post_type ) {
  case 'cpt-inspire':
    get_template_part( 'template-parts/loops/inspiration' );
    break;

  case 'post':
    get_template_part( 'template-parts/loops/post' );
    break;
}
