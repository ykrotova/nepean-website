/*global Wynstan*/

import '../../node_modules/overlayscrollbars/css/OverlayScrollbars.min.css';
import '../../node_modules/overlayscrollbars/js/overlayScrollbars.min.js';
// import '../../node_modules/video.js/dist/video.min.js';
// import '../../node_modules/video.js/dist/video-js.min.css';
// import videojs from 'video.js';
// window.videojs = videojs;
import './woocommerce';
import '../scss/main.scss';

jQuery(document).ready(function($) {
  var body = $('body');
  var header = $('.header');
  var screen_admin_min = 783;
  var adminBarHeight = 0;

  // Calculate admin bar
  if (body.hasClass('admin-bar')) {
    if ($(window).width() > screen_admin_min) {
      adminBarHeight = 32;
    } else {
      adminBarHeight = 46;
    }
  }

  // Go to element
  $.fn.goToElement = function(offset) {
    offset = 70;
    if ($(this).length) {
      var offsetTop = $(this).offset().top,
        args = {
          scrollTop: (offsetTop - adminBarHeight - offset)
        };
      $('html, body').animate(args, 'fast');
    }
    return this;
  };
  body.on('click', '.anchor-link', function(e) {
    var _this = $(this);
    if (0 === _this.attr('href').indexOf('#')) {
      e.preventDefault();
      $(_this.attr('href')).goToElement();
    }
  });

  // Window events
  const onWindowEvents = function(foo, isReady, isLoad, isResize, isScroll) {
    if (isReady) {
      $(document).ready(foo);
    }

    if (isLoad) {
      $(window).bind('load', foo);
    }

    if (isResize) {
      var throttleResizing = _.throttle(foo, 100);
      $(window).bind('resize', throttleResizing);
    }

    if (isScroll) {
      var throttleScroll = _.throttle(foo, 10);
      $(window).bind('scroll', throttleScroll);
    }
  };

  // Balance elements
  var balanceElements = function(container, clr, gapDelta) {
    clr = (typeof clr !== 'undefined' ? clr : false);
    gapDelta = (typeof gapDelta !== 'undefined' ? gapDelta : 10);
    var currentTallest = 0,
      currentRowStart = 0,
      rowDivs = [],
      el,
      currentDiv,
      topPosition = 0;
    var c = $(container).filter(':visible');
    if (!c.length) {
      return false;
    }
    if (!clr) {
      c.css('height', 'auto');
    } else {
      c.removeAttr('style');
    }
    c.each(function() {
      el = $(this);
      topPosition = el.offset().top;
      if ((currentRowStart < (topPosition + gapDelta)) && (currentRowStart > (topPosition - gapDelta))) {
        rowDivs.push(el);
        currentTallest = (currentTallest < el.outerHeight()) ? (el.outerHeight()) : (currentTallest);
      } else {
        for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
          rowDivs[currentDiv].css('height', currentTallest + 'px');
        }
        rowDivs.length = 0; // empty the array
        currentRowStart = topPosition;
        currentTallest = el.outerHeight();
        rowDivs.push(el);
      }
      for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
        rowDivs[currentDiv].css('height', currentTallest + 'px');
      }
    });
    return true;
  };

  // Offcanvas toggle
  $('.offcanvas__toggle').on('click', function(e) {
    e.preventDefault();
    $('body').toggleClass('offcanvas-active');
    $('.sub-menu').removeClass('sub-menu--active');
  });

  $('.offcanvas .menu-item-has-children > a').on('click', function(e) {
    e.preventDefault();
    $(this).closest('.menu-item-has-children').find('.sub-menu').toggleClass('sub-menu--active');
  });

  $('.offcanvas .menu-item-back a').on('click', function(e) {
    e.preventDefault();
    $(this).closest('.sub-menu').removeClass('sub-menu--active');
  });

  // Header menu item hover
  setTimeout(function() {
    // Wait for max mega menu to load first
    $('.custom-mega > ul.mega-sub-menu li.mega-menu-item').hoverIntent({
      over: function() {
        $(this).addClass('hover');
      },
      out: function() {
        $(this).removeClass('hover');
      },
      timeout: 200
    });
  }, 1000);

  // Disable collapse toggle for mega menu on desktop
  $(document).on('click', '.custom-mega a.mega-menu-link', function(e) {
    const _this = $(this);
    if ((_this.attr('href') !== '#') && $(window).width() >= 1200) {
      e.preventDefault();
      _this.closest('.custom-mega').removeClass('mega-toggle-on');
      location.href = _this.attr('href');
    }
  });

  // Modal trigger for Elementor button
  $(document).on('click', 'div[data-toggle="modal"] a', function(e) {
    e.preventDefault();
  });

  // In case other modals are still open
  $('.modal').on('hidden.bs.modal', function() {
    if ($('.modal').hasClass('show')) {
      body.addClass('modal-open');
    }
  });

  // Scrollbar for showrooms
  if ($('#wpsl-stores').length) {
    OverlayScrollbars($('#wpsl-stores'), {});

    $(document).on('change', '#wpsl-search-input', function() {
      $('#wpsl-search-btn').trigger('click');
    });
  }

  // Inspirations
  $(document).on('change', '.inspirations__wrapper .inspirations__select', function() {
    const postsWrapper = $('.inspirations__wrapper');
    const url = this.value;

    postsWrapper.find('.alm-load-more-btn').addClass('loading');
    postsWrapper.find('.alm-masonry').hide();

    $('<div>').load(url, function() {
      $(this).find('.inspirations__posts .alm-load-more-btn').addClass('loading');
      postsWrapper.html($(this).find('.inspirations__wrapper').html());

      history.pushState({}, '', url);

      if ('almInit' in window) {
        window.almInit(document.getElementById('ajax-load-more'));

        postsWrapper.removeClass('loading');
      }
    });
  });

  // Balance elements
  const balanceAll = function() {
    balanceElements($('.balance-elements'), false, 30);
    balanceElements($('.woocommerce ul.products li.product .woocommerce-loop-product__title'), false, 30);
    balanceElements($('.woocommerce ul.products li.product .colors-palette'), false, 30);
    balanceElements($('.woocommerce ul.products li.product .price'), false, 30);
  };

  $(document).on('yith-wcan-ajax-filtered', balanceAll);

  onWindowEvents(balanceAll, 1, 1, 1, 0);

  // Slick functions
  // Homepage slick slider
  const homeSlick = function() {
    // Get amount of timeline date items
    const timelineCount = $('.home-slider__text-item').length;

    // Divide that by 100 to get the individual date heights
    let timelineIncrement = 100 / timelineCount;

    // Set the default height of the first timeline date item
    $('.home-slider__line-inner').css('height', timelineIncrement + '%');

    // slick text slider initiation
    $('.home-slider__text').slick({
      infinite: false,
      variableWidth: false,
      draggable: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      dots: false,
      vertical: true,
      fade: false,
      asNavFor: $('.home-slider__image'),
    });

    // slick text slider initiation
    $('.home-slider__image').slick({
      infinite: false,
      variableWidth: false,
      draggable: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      dots: false,
      vertical: true,
      fade: false,
    });

    // Slick gotoslide on click function
    $('.home-slider__text a[data-slide]').click(function(e) {
      e.preventDefault();

      // Data slide
      let slideno = $(this).data('slide');

      // Sets the variable to the new height
      let lineHeight = timelineIncrement * slideno;

      // Navigates the the relevant slide
      $('.home-slider__text').slick('slickGoTo', slideno - 1);

      // Sets the new height of the line
      $('.home-slider__line-inner').css('height', lineHeight + '%');
    });
  };

  // Timeline slick slider
  const timelineSlick = function() {

    // Get amount of timeline date items
    const timelineCount = $('.timeline__dates-item').length;

    // Divide that by 100 to get the individual date heights
    let timelineIncrement = 100 / timelineCount;

    // Set the default height of the first timeline date item
    $('.timeline__line-inner').css('height', timelineIncrement + '%');

    // slick text slider initiation
    $('.timeline__image').slick({
      infinite: false,
      variableWidth: false,
      draggable: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      dots: false,
      vertical: true,
      verticalSwiping: true,
    });

    // Slick gotoslide on click function
    $('.timeline__dates a[data-slide]').click(function(e) {
      e.preventDefault();

      // Data slide
      let slideno = $(this).data('slide');

      // Sets the variable to the new height
      let lineHeight = timelineIncrement * slideno;

      // Navigates the the relevant slide
      $('.timeline__image').slick('slickGoTo', slideno - 1);

      // Sets the correct class for the date item
      $('.timeline__dates a').removeClass('timeline__dates-item--active');
      $(this).addClass('timeline__dates-item--active');

      // Sets the new height of the line
      $('.timeline__line-inner').css('height', lineHeight + '%');

      // If line is 100% height then add full height class
      if (lineHeight >= 100) {
        $('.timeline__line').addClass('timeline__line--full');
      } else {
        $('.timeline__line').removeClass('timeline__line--full');
      }
    });
  };

  // Carousel slick slider
  if ($('.home-slider__text').length && $('.home-slider__image').length) {
    homeSlick();
  }

  if ($('.timeline__dates').length && $('.timeline__image').length) {
    timelineSlick();
  }

  const beInspireCarousel = $('.be-inspired .carousel-items');
  if (beInspireCarousel.length) {
    beInspireCarousel.slick({
      infinite: false,
      draggable: true,
      swipeToSlide: true,
      variableWidth: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: false,
      dots: false,
      responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 3,
            variableWidth: false,
            dots: true
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 2,
            variableWidth: false,
            dots: true,
          }
        },
        {
          breakpoint: 576,
          settings: {
            slidesToShow: 1,
            variableWidth: false,
            dots: true,
          }
        }
      ]
    });

    const timelineCount = $('.be-inspired .nav-item').length;
    let timelineIncrement = 100 / timelineCount;

    $('.be-inspired__line-inner').css('height', timelineIncrement + '%');

    $('.be-inspired a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
      // e.target;
      // e.relatedTarget;

      let lineHeight = timelineIncrement * parseInt($(e.target).data('id'));

      $('.carousel-items').slick('setPosition');

      $('.be-inspired__line').removeClass(function(index, className) {
        return (className.match(/(^|\s)be-inspired__line--\S+/g) || []).join(' ');
      });

      $('.be-inspired__line-inner').css('height', lineHeight + '%');
    });
  }

  // Consult form val select
  if ($('#consult-popup-select').length) {
    body.on('click', '[data-elementor-type="footer"] [data-target="#modal-free-consultant"]', function(e) {
      e.preventDefault();

      setTimeout(function() {
        $('.select-wrap.consult-type select').val($('#consult-popup-select').val());
      }, 100);
    });
  }

  if ($('.wow').length) {
    // WOW effects
    new WOW().init();
  }

  // Manual video style
  /*const loadVideo = function() {
    videojs(document.querySelector('.video-js--manual'), {
      responsive: true,
      fluid: true,
      controls: true,
      autoplay: false,
      preload: 'auto'
    });
  };

  window.almComplete = function(alm) {
    setTimeout(function() {
      loadVideo();
    }, 300);
  };*/

  // Trigger live chat
  $(document).on('click', '.trigger-chat, .trigger-chat a', function(e) {
    e.preventDefault();
    e.stopPropagation();

    if ('tidioChatApi' in window) {
      window.tidioChatApi.open();
    }
  });

  // Trigger free free consultant modal
  $(document).on('click', '.trigger-modal-free-consultant, .trigger-modal-free-consultant a', function(e) {
    $('#modal-free-consultant').modal('show');
  });

  // Ajax load inspirations
  $('body').on('show.bs.modal','.modal--be-inspired',function(){
      let thisModal = $(this);

      if (thisModal.hasClass('loaded')) {
        return;
      }

      $.ajax({
        type: 'GET',
        url: Wynstan.ajaxUrl,
        dataType: 'json',
        data: {
          action: 'show_inspiration_modal_body',
          postId: thisModal.find('.modal-body').data('post-id'),
        },
        success: function(response) {
          if (response.success) {
            thisModal.find('.modal-body').html(response.data.html);
          } else {
            thisModal.find('.modal-body').text('Something went wrong, please try again later!');
          }

          thisModal.addClass('loaded');
        },
      });
  });

  // $('.modal--be-inspired').on('show.bs.modal', function() {
  //   let thisModal = $(this);
  //
  //   if (thisModal.hasClass('loaded')) {
  //     return;
  //   }
  //
  //   $.ajax({
  //     type: 'GET',
  //     url: Wynstan.ajaxUrl,
  //     dataType: 'json',
  //     data: {
  //       action: 'show_inspiration_modal_body',
  //       postId: thisModal.find('.modal-body').data('post-id'),
  //     },
  //     success: function(response) {
  //       if (response.success) {
  //         thisModal.find('.modal-body').html(response.data.html);
  //       } else {
  //         thisModal.find('.modal-body').text('Something went wrong, please try again later!');
  //       }
  //
  //       thisModal.addClass('loaded');
  //     },
  //   });
  // });
});
