/* global Wynstan,wc_add_to_cart_variation_params,wc_add_to_cart_params,wc_single_product_params */
jQuery(document).ready(function($) {
  const Woo = window['Woo'] || {};
  const body = $('body');
  const mobileScreen = 768;

  Woo.requests = [];

  Woo.init = () => {
    // Toggle cart link
    $(document).on('click', '.woo-cart__toggle, .woo-cart__toggle a, a[href$="/cart/"]:not(.edit-cart), a[href$="/cart"]:not(.edit-cart)', function(e) {
      e.preventDefault();

      Woo.toggleCart();
    });

    // Archive page with sidebar filter
    (function(enable) {
      if (!enable) {
        return;
      }

      // Overlay scroll
      const applyScroll = function() {
        OverlayScrollbars($('.archive-with-sidebar:not(.archive-spare-parts) .yith-woocommerce-ajax-product-filter > ul'), {});
      };

      applyScroll();

      $(document).on('yith-wcan-ajax-filtered', applyScroll);

      // Default collapsed sidebar on mobile
      if ($(window).width() < mobileScreen) {
        $('.yith-woocommerce-ajax-product-filter .title-toggle').addClass('collapsed');
      }

      // Toggle sidebar
      ($(document).on('click', '.archive-with-sidebar .yith-woocommerce-ajax-product-filter .title', function(e) {
        e.preventDefault();

        const list = $(this).closest('.yith-woocommerce-ajax-product-filter').find('ul.yith-wcan');
        if (list.is(':visible')) {
          $(this).find('.title-toggle').addClass('collapsed');
        } else {
          $(this).find('.title-toggle').removeClass('collapsed');
        }

        list.slideToggle();
      }));
    })(body.hasClass('archive-with-sidebar'));

    // Cart
    (function(enable) {
      if (!enable) {
        return;
      }

      $(document).on('change', '[name="calc_shipping_state"]', function() {
        $(this).closest('form').find('[name="calc_shipping"]').trigger('click');
      });
    })(body.hasClass('woocommerce-cart'));

    // Single product pages
    (function(enable) {
      if (!enable) {
        return;
      }
      OverlayScrollbars($('.woo-images'), {});
    })(body.hasClass('single-product'));

    // Page with product ajax
    (function(enable) {
      if (!enable) {
        return;
      }

      $(document).on('change', '#ajax-modal-product .woo-custom-checkboxes--variations', function() {
        $('[name="' + $(this).data('target') + '"]').val($(this).val()).trigger('change');

        Woo.cleanUpCheckedClasses();
      });

      // To unselect
      $(document).on('click', '#ajax-modal-product .woo-custom-checkboxes--variations[type="radio"]:checked', function(e) {
        if ($(this).next('.radio-label').hasClass('checked')) {
          $('[name="' + $(this).data('target') + '"]').val('').trigger('change');
          Woo.cleanUpCheckedClasses();
        }
      });

      const handleAttributeChanged = function() {
        const selects = $('#ajax-modal-product select[name^="attribute_"]');
        selects.each(function() {
          const select = $(this);
          const options = select.find('option');
          const name = select.attr('name');
          const target = $('[name="target-' + name + '"]');

          target.prop('disabled', true);

          options.each(function() {
            const option = $(this);
            const value = option.attr('value');

            if ('' !== value.trim()) {
              $('[name="target-' + name + '"][value="' + value + '"]').prop('disabled', false);
            }
          });

          if (!select.val()) {
            target.prop('checked', false);
          }
        });

        $('#ajax-modal-product .woo-custom-checkboxes--variations:disabled').prop('checked', false);
      };

      $(document).on('change', '#ajax-modal-product select[name^="attribute_"]', handleAttributeChanged);

      $(document).on('update_variation_values', '#ajax-modal-product form.variations_form', handleAttributeChanged);

      $(document).on('click', '#ajax-modal-product .single_add_to_cart_button', function(e) {
        e.preventDefault();
        const _this = $(this);
        const form = _this.closest('form');
        const productID = form.find('[name="variation_id"]').length ? form.find('[name="variation_id"]').val() : form.find('[name="product_id"]').val();
        const qty = form.find('[name="quantity"]').val();

        Woo.ajaxAddToCart(_this, productID, qty);
      });

      $(document).on('click', 'li.product.product-type--fabric a.woocommerce-loop-product__link, li.product.product-type--fabric a.add_to_cart_button', function(e) {
        e.preventDefault();
        const productID = $(this).closest('li.product').find('a.add_to_cart_button').data('product_id');

        Woo.toggleProductAjaxModal(productID);
      });
    })($('#ajax-modal-product').length > 0);
  };

  Woo.cleanUpCheckedClasses = () => {
    $('#ajax-modal-product .woo-custom-checkboxes--variations').next('.radio-label').removeClass('checked');
    $('#ajax-modal-product .woo-custom-checkboxes--variations:checked').next('.radio-label').addClass('checked');
  };

  Woo.toggleProductAjaxModal = (productId) => {
    Woo.addRequest({
      type: 'POST',
      url: Wynstan.ajaxUrl,
      data: {
        action: 'get_ajax_products',
        product_id: productId,
      },
      beforeSend: function() {
        $('#ajax-modal-product--content').html('<p class="py-5 text-center"><img src="/wp-content/themes/wynstan-2021/assets/images/loading.svg" class="img-fluid" width="80" alt="loading"><span class="sr-only">Loading...</span></p>');
      },
      success: function(response) {
        if (!response.success) {
          alert('Something went wrong, please try again later');

          $('#ajax-modal-product').modal('hide');
        } else {
          $('#ajax-modal-product--content').html(response.data.html);
          Woo.initializeProductScripts('#ajax-modal-product--content');
        }
      },
      dataType: 'json',
    });

    $('#ajax-modal-product').modal('show');
  };

  Woo.initializeProductScripts = function(wrapper = '') {
    if (typeof wc_add_to_cart_variation_params !== 'undefined') {
      $(wrapper + ' .variations_form').each(function() {
        $(this).wc_variation_form();
      });
    }

    if (typeof wc_single_product_params !== 'undefined') {
      $(wrapper + ' .woocommerce-product-gallery').each(function() {
        $(this).trigger('wc-product-gallery-before-init', [this, wc_single_product_params]);
        $(this).wc_product_gallery(wc_single_product_params);
        $(this).trigger('wc-product-gallery-after-init', [this, wc_single_product_params]);
      });
    }

    Woo.cleanUpCheckedClasses();
  };

  Woo.toggleCart = (action = 'toggle') => {
    $('#woo-cart-sidebar').modal(action);
  };

  Woo.is_blocked = function($node) {
    return $node.is('.processing') || $node.parents('.processing').length;
  };

  Woo.block = function($node) {
    if (!Woo.is_blocked($node)) {
      $node.addClass('processing').block({
        message: null,
        overlayCSS: {
          background: '#fff',
          opacity: 0.6,
        },
      });
    }
  };

  Woo.unblock = function($node) {
    $node.removeClass('processing').unblock();
  };

  Woo.ajaxAddToCart = (el, productId, qty = 1) => {
    const _this = $(el);

    if (!productId) {
      return false;
    }

    const $form = $('.woocommerce-cart-form');
    const data = {
      'product_id': productId,
      'quantity': qty
    };

    // Trigger event.
    $(document.body).trigger('adding_to_cart', [_this, data]);

    Woo.addRequest({
      type: 'POST',
      url: wc_add_to_cart_params.wc_ajax_url.toString().replace('%%endpoint%%', 'add_to_cart'),
      data: data,
      beforeSend: function() {
        _this.removeClass('added');
        _this.addClass('adding');
      },
      success: function(response) {
        if (!response) {
          return;
        }

        if (response.error && response.product_url) {
          Woo.block($form);

          $(document.body).one('wc_fragments_refreshed', () => {
            Woo.unblock($form);
            Woo.unblock($('div.cart_totals'));
            $.scroll_to_notices($('[role="alert"]'));
          });

          $(document.body).trigger('wc_fragment_refresh');

          Woo.toggleCart('show');

          return;
        }

        // Redirect to cart option
        if (wc_add_to_cart_params.cart_redirect_after_add === 'yes') {
          window.location = wc_add_to_cart_params.cart_url;
          return;
        }

        $(document.body).trigger('added_to_cart', [response.fragments, response.cart_hash, _this]);

        Woo.toggleCart('show');
      },
      complete: function() {
        _this.addClass('added');
        _this.removeClass('adding');
      },
      dataType: 'json',
    });
  };

  Woo.addRequest = function(request) {
    Woo.requests.push(request);

    if (1 === Woo.requests.length) {
      this.run();
    }
  };

  Woo.run = function() {
    const originalCallback = Woo.requests[0].complete;

    Woo.requests[0].complete = function() {
      if (typeof originalCallback === 'function') {
        originalCallback();
      }

      Woo.requests.shift();

      if (Woo.requests.length > 0) {
        Woo.run();
      }
    };

    $.ajax(this.requests[0]);
  };

  // No code should be added below this
  window['Woo'] = Woo;
  Woo.init();
});
