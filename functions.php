<?php

require_once 'inc/setup.php';
require_once 'inc/woocommerce.php';
require_once 'inc/modules/elementor/elementor.php';

function the_posted_on_date() {
  $time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
  if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
    $time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
  }

  $time_string = sprintf( $time_string,
    esc_attr( get_the_date( 'c' ) ),
    esc_html( get_the_date() ),
    esc_attr( get_the_modified_date( 'c' ) ),
    esc_html( get_the_modified_date() )
  );

  $posted_on = sprintf(
  /* translators: %s: post date. */
    esc_html_x( 'Posted on %s', 'post date', 'wynstan' ),
    '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
  );

  $byline = sprintf(
  /* translators: %s: post author. */
    esc_html_x( 'by %s', 'post author', 'wynstan' ),
    '<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
  );

  echo '<span class="posted-on">' . $posted_on . '</span><span class="byline"> ' . $byline . '</span>'; // WPCS: XSS OK.
}

function get_assets_path( $filename = '' ) {
  $dist_path = get_template_directory_uri() . '/assets/';

  if ( empty( $filename ) ) {
    return $dist_path;
  }

  $directory = dirname( $filename ) . '/';
  $file      = basename( $filename );

  return esc_url( $dist_path . $directory . $file );
}

function the_assets_path( $filename ) {
  echo get_assets_path( $filename );
}

function get_page_id_by_template( $template_file_name ) {
  $pages = get_posts( array(
    'post_type'      => 'page',
    'posts_per_page' => - 1,
    'offset'         => 0,
    'orderby'        => 'date',
    'order'          => 'DESC',
    'post_status'    => 'publish',
    'meta_key'       => '_wp_page_template',
    'meta_value'     => $template_file_name,
  ) );
  if ( isset( $pages[0] ) ) {
    return $pages[0]->ID;
  }

  return false;
}

function get_page_title() {
  if ( is_home() ) {
    if ( get_option( 'page_for_posts', true ) ) {
      return get_the_title( get_option( 'page_for_posts', true ) );
    } else {
      return __( 'Latest Posts', 'sage' );
    }
  } elseif ( is_archive() ) {
    return get_the_archive_title();
  } elseif ( is_search() ) {
    return sprintf( __( 'Search Results for %s', 'sage' ), get_search_query() );
  } elseif ( is_404() ) {
    return __( 'Not Found', 'sage' );
  } else {
    return get_the_title();
  }
}

function get_last_term( $post_id, $taxonomy ) {
  if ( ! $post = get_post( $post_id ) ) {
    return false;
  }

  if ( class_exists( 'WPSEO_Primary_Term' ) ) {
    $term    = new WPSEO_Primary_Term( $taxonomy, $post->ID );
    $term_id = $term->get_primary_term();

    if ( $term_id ) {
      return get_term( $term_id, $taxonomy );
    }
  }

  $terms = get_the_terms( $post->ID, $taxonomy );
  if ( ! is_wp_error( $terms ) && ! empty( $terms ) ) {
    return array_pop( $terms );
  }

  return null;
}

function get_related_posts( $post_id, $related_count, $args = array() ) {
  $terms = get_the_terms( $post_id, 'category' );

  if ( empty( $terms ) ) {
    $terms = array();
  }

  $term_list = wp_list_pluck( $terms, 'slug' );

  $related_args = array(
    'post_type'      => 'post',
    'posts_per_page' => $related_count,
    'post_status'    => 'publish',
    'post__not_in'   => array( $post_id ),
    'orderby'        => 'rand',
    'tax_query'      => array(
      array(
        'taxonomy' => 'category',
        'field'    => 'slug',
        'terms'    => $term_list,
      ),
    ),
  );

  $related_posts = get_posts( $related_args );

  if ( count( $related_posts ) < $related_count ) {
    $other_args = array(
      'post_type'      => 'post',
      'posts_per_page' => ( $related_count - count( $related_posts ) ),
      'post_status'    => 'publish',
      'post__not_in'   => array_merge( array( $post_id ), wp_list_pluck( $related_posts, 'ID' ) ),
      'orderby'        => 'rand',
    );

    $related_posts = array_merge( $related_posts, get_posts( $other_args ) );
  }

  return $related_posts;
}

function get_sibling( $post_id, $link = 'next' ) {
  if ( ! empty( $current_post = get_post( $post_id ) ) ) {
    $all_posts = get_posts( [
      'posts_per_page' => - 1,
      'post_type'      => $current_post->post_type,
      'post_parent'    => $current_post->post_parent,
    ] );

    if ( 1 == count( $all_posts ) ) {
      return false;
    }

    foreach ( $all_posts as $index => $p ) {
      if ( $p->ID == $current_post->ID ) {
        switch ( $link ) {
          case 'prev':
          case 'before':
            if ( $index == 0 ) {
              return false;
            }

            return $all_posts[ $index - 1 ];
            break;

          default:
            if ( $index < count( $all_posts ) - 1 ) {
              return $all_posts[ $index + 1 ];
            }

            return false;
            break;
        }
      }
    }
  }

  return false;
}

function get_adjacent_taxonomy_id( $taxonomy, $current_term_id, $previous = false ) {
  $term_ids = get_terms( [ 'hide_empty' => true, 'taxonomy' => $taxonomy, 'fields' => 'ids' ] );

  if ( is_wp_error( $term_ids ) || empty( $term_ids ) ) {
    return false;
  }

  $term_ids = array_values( $term_ids );

  $current_index = array_search( $current_term_id, $term_ids );

  if ( ! $previous ) {
    if ( false !== $current_index && isset( $term_ids[ $current_index + 1 ] ) ) {
      return $term_ids[ $current_index + 1 ];
    }
  } else {
    if ( false !== $current_index && isset( $term_ids[ $current_index - 1 ] ) ) {
      return $term_ids[ $current_index - 1 ];
    }
  }

  return false;
}

function is_elementor_active() {
  return class_exists( 'Elementor\Plugin' );
}

function woo_is_product_attribute_pages() {
  $key = md5( 'woo_is_product_attribute_pages' );

  if ( ! $result = wp_cache_get( $key ) ) {
    $result = is_product_taxonomy() && ! is_product_category();

    wp_cache_set( $key, $result );
  }

  return $result;
}

function woo_is_fabric_sample_terms() {
  $key = md5( 'woo_is_fabric_sample_terms' );

  if ( ! $result = wp_cache_get( $key ) ) {
    $result = (
      is_product_category( CAT_FABRIC ) ||
      is_tax( TERM_FABRIC )
    );

    wp_cache_set( $key, $result );
  }

  return $result;
}

function woo_is_spare_parts_terms() {
  $key = md5( 'woo_is_spare_parts_terms' );

  if ( ! $result = wp_cache_get( $key ) ) {
    $result = (
      is_product_category( CAT_SPARE_PARTS ) ||
      is_tax( TERM_SPARE_PARTS )
    );

    wp_cache_set( $key, $result );
  }

  return $result;
}

function woo_has_products_filter_sidebar() {
  $key = md5( 'woo_has_products_filter_sidebar' );

  if ( ! $result = wp_cache_get( $key ) ) {
    $result = (
      woo_is_fabric_sample_terms() ||
      woo_is_spare_parts_terms()
    );

    wp_cache_set( $key, $result );
  }

  return $result;
}

function woo_has_products_filter_topbar() {
  $key = md5( 'woo_has_products_filter_topbar' );

  if ( ! $result = wp_cache_get( $key ) ) {
    $result = (
      woo_is_product_attribute_pages() &&
      ! woo_is_fabric_sample_terms() &&
      ! woo_is_spare_parts_terms()
    );

    wp_cache_set( $key, $result );
  }

  return $result;
}

function woo_has_archive_banner() {
  $key = md5( 'woo_has_archive_banner' );

  if ( ! $result = wp_cache_get( $key ) ) {
    $result = (
      woo_is_product_attribute_pages() &&
      ! woo_is_fabric_sample_terms() &&
      ! woo_is_spare_parts_terms()
    );

    wp_cache_set( $key, $result );
  }

  return $result;
}

function woo_has_fabric_modal() {
  return is_post_type_archive( 'cpt-inspire' ) || is_woocommerce() || is_front_page();
}

function woo_tabs_html( $tabs ) {
  ob_start();
  ?>
  <div class="tabs-showroom">
    <ul class="tabs-showroom__nav nav nav-pills mb-3" role="tablist">
      <?
      $index = 0;

      foreach ( $tabs as $key => $tab ) : $index ++; ?>
        <li class="nav-item">
          <a class="nav-link nav-link--<?= $index ?> <?= ( 1 == $index ) ? 'active' : '' ?>" data-toggle="pill" href="#woo-tab-<?= esc_attr( $key ) ?>" role="tab" aria-controls="woo-tab-<?= esc_attr( $key ) ?>" aria-selected="true">
            <?= wp_strip_all_tags( $tab['label'] ) ?>
          </a>
        </li>
      <? endforeach; ?>
    </ul>

    <div class="tab-content pt-3">
      <?
      $index = 0;

      foreach ( $tabs as $key => $tab ) : $index ++; ?>
        <div class="tab-pane fade <?= 1 == $index ? 'active show' : '' ?>" id="woo-tab-<?= esc_attr( $key ) ?>" role="tabpanel">
          <?= $tab['content'] ?>
        </div>
      <? endforeach; ?>
    </div>
  </div>
  <?php

  return ob_get_clean();
}

function woo_product_is( $_product, $category_terms ) {
  if ( ! $_product ) {
    return false;
  }

  if ( is_numeric( $_product ) ) {
    $_product = wc_get_product( $_product );
  }

  if ( is_string( $category_terms ) ) {
    $category_terms = [ $category_terms ];
  }

  $product_id = $_product->is_type( 'variation' ) ? $_product->get_parent_id() : $_product->get_id();

  return has_term( $category_terms, 'product_cat', $product_id );
}

function woo_product_is_fabric_samples( $_product ) {
  return woo_product_is( $_product, CAT_FABRIC );
}

function woo_product_is_wynstan_health( $_product ) {
  return has_term( 'wynstan-health', TERM_SPARE_PARTS, $_product );
}

function woo_product_is_spare_parts( $_product ) {
  return woo_product_is( $_product, CAT_SPARE_PARTS );
}

function woo_product_is_normal_product( $_product ) {
  return woo_product_is( $_product, CAT_NORMAL_PRODUCTS );
}

function woo_get_cc_emails_by_postcode( $post_code ) {
  $cc_emails = array();

  if ( $post_code ) {
    $args = array(
      'post_type'      => 'wpsl_stores',
      'post_status'    => 'publish',
      'posts_per_page' => 1,
      'meta_query'     => array(
        array(
          'key'     => 'serviced_postcodes',
          'value'   => $post_code,
          'compare' => 'LIKE',
        ),
      ),
    );

    $stores = get_posts( $args );

    if ( ! empty( $stores ) ) {
      foreach ( $stores as $store ) {
        $store_emails = get_post_meta( $store->ID, 'wpsl_email', true );

        if ( ! empty( $store_emails ) ) {
          $cc_emails[] = $store_emails;
        }
      }
    }
  }

  return $cc_emails;
}

function woo_get_samples_quantity_in_cart() {
  $sample_quantity = 0;

  foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
    if ( woo_product_is_fabric_samples( $cart_item['data'] ) ) {
      $sample_quantity += $cart_item['quantity'];
    }
  }

  return $sample_quantity;
}

function woo_validate_fabric_in_cart( $product_id, $quantity_to_add, $throw = true ) {
  if ( $quantity_to_add <= 0 || ! woo_product_is_fabric_samples( $product_id ) ) {
    return true;
  }

  $cart_contents    = WC()->cart->get_cart_contents();
  $count_per_fabric = [];

  foreach ( $cart_contents as $cart_item_key => $cart_item ) {
    $variation_id = isset($cart_item['variation_id']) ? $cart_item['variation_id'] : $cart_item['product_id'];

    if ( ! woo_product_is_fabric_samples( $variation_id ) ) {
      continue;
    }

    if ( isset( $count_per_fabric[ $variation_id ] ) ) {
      $count_per_fabric[ $variation_id ] += $cart_item['quantity'];
    } else {
      $count_per_fabric[ $variation_id ] = $cart_item['quantity'];
    }
  }

  if (
    ( isset( $count_per_fabric[ $product_id ] ) && ( $count_per_fabric[ $product_id ] + $quantity_to_add > MAX_SAMPLES_PER_PRODUCT ) ) ||
    ( ! isset( $count_per_fabric[ $product_id ] ) && $quantity_to_add > MAX_SAMPLES_PER_PRODUCT )
  ) {
    $qty_left = MAX_SAMPLES_PER_PRODUCT - ( isset( $count_per_fabric[ $product_id ] ) ? $count_per_fabric[ $product_id ] : 0 );

    $product_colour = wc_get_product( $product_id )->get_attribute( TERM_COLOUR );
    $product_title  = get_the_title( $product_id ) . ( ! empty( $product_colour ) ? " | $product_colour" : '' );

    if ( $qty_left > 0 ) {
      $msg = 'You can only add another ' . $qty_left . ' "' . $product_title . '" ' . _n( 'sample', 'samples', $qty_left ) . ' to your cart';
    } else {
      $msg = 'Maximum of ' . MAX_SAMPLES_PER_PRODUCT . ' "' . $product_title . '" samples has reached, you cannot add another "' . $product_title . '" sample to your cart.';
    }
  } elseif ( array_sum( $count_per_fabric ) + $quantity_to_add > MAX_SAMPLES_ALL_PRODUCT ) {
    $qty_left = MAX_SAMPLES_ALL_PRODUCT - array_sum( $count_per_fabric );

    if ( $qty_left > 0 ) {
      $msg = 'You can only add another ' . $qty_left . ' ' . _n( 'sample', 'samples', $qty_left ) . ' to your cart';
    } else {
      $msg = 'Maximum of ' . MAX_SAMPLES_ALL_PRODUCT . ' samples has reached, you cannot add another one to your cart.';
    }
  }

  if ( ! empty( $msg ) ) {
    if ( $throw ) {
      throw new Exception( __( $msg, 'woocommerce' ) );
    } else {
      wc_add_notice( __( $msg, 'woocommerce' ), 'error' );
    }

    return false;
  }

  return true;
}

function woo_find_product_variations_contain_meta_key( $parent_product_id, $meta_key ) {
  global $wpdb;

  // Get the attributes of the variations.
  $query = $wpdb->prepare(
    "
			SELECT postmeta.post_id, postmeta.meta_key, postmeta.meta_value, posts.menu_order FROM {$wpdb->postmeta} as postmeta
			LEFT JOIN {$wpdb->posts} as posts ON postmeta.post_id=posts.ID
			WHERE postmeta.post_id IN (
				SELECT ID FROM {$wpdb->posts}
				WHERE {$wpdb->posts}.post_parent = %d
				AND {$wpdb->posts}.post_status = 'publish'
				AND {$wpdb->posts}.post_type = 'product_variation'
			)
			",
    $parent_product_id
  );

  $query .= ' AND postmeta.meta_key = \'' . sanitize_key( $meta_key ) . '\'';

  $query .= ' ORDER BY posts.menu_order ASC, postmeta.post_id ASC;';

  $meta_data = $wpdb->get_results( $query ); // phpcs:ignore WordPress.DB.PreparedSQL.NotPrepared

  if ( ! $meta_data ) {
    return false;
  }

  return $meta_data;
}
