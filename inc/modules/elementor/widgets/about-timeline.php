<?php

defined( 'ABSPATH' ) or exit;

use Elementor\Controls_Manager;
use Elementor\Repeater;
use Elementor\Utils;
use Elementor\Widget_Base;

class About_Timeline extends Widget_Base {
  public function get_name() {
    return 'About_Timeline';
  }

  public function get_title() {
    return 'Timeline';
  }

  public function get_icon() {
    return 'fa fa-puzzle-piece';
  }

  public function get_categories() {
    return [ 'custom' ];
  }

  protected function _register_controls() {
    $this->start_controls_section(
      'content_section',
      [
        'label' => 'Content',
        'tab'   => Controls_Manager::TAB_CONTENT,
      ]
    );

    // Add controls here
    $repeater = new Repeater();

    $repeater->add_control(
      'heading',
      [
        'label' => __( 'Heading', 'wynstan' ),
        'type'  => Controls_Manager::TEXT,
      ]
    );

    $repeater->add_control(
      'image',
      [
        'label'    => __( 'Image', 'wynstan' ),
        'type'     => Controls_Manager::MEDIA,
        'default'  => [
          'url' => Utils::get_placeholder_image_src(),
        ],
        'required' => true,
      ]
    );

    $repeater->add_control(
      'content',
      [
        'label'      => __( 'Content', 'wynstan' ),
        'type'       => Controls_Manager::WYSIWYG,
        'show_label' => false,
      ]
    );

    $this->add_control(
      'list',
      [
        'label'       => __( 'Timeline', 'wynstan' ),
        'type'        => Controls_Manager::REPEATER,
        'fields'      => $repeater->get_controls(),
        'title_field' => '{{{ heading }}}',
      ]
    );

    $this->end_controls_section();
  }

  protected function render() {
    $list = $this->get_settings_for_display( 'list' );

    $uid = uniqid( 'about-timeline-' );

    if ( ! empty( $list ) ) : ?>
      <div id="<?= $uid ?>" class="overflow-hidden">
        <div class="row timeline">
          <div class="col-lg-3 col-12 text-center">
            <div class="timeline__dates">
              <? foreach ( $list as $index => $item ) : ?>
                <a href="#" class="timeline__dates-item <?= 0 == $index ? 'timeline__dates-item--active' : '' ?>" data-slide="<?= $index + 1 ?>"><?= $item['heading'] ?></a>
              <? endforeach; ?>

              <div class="timeline__line"><span class="timeline__line-inner"></span></div>
            </div>
          </div>

          <div class="col-lg-9 col-12 text-center">
            <div class="timeline__image-wrap">
              <div class="timeline__image">
                <? foreach ( $list as $index => $item ) : ?>
                  <div>
                    <div class="timeline__image-item">
                      <div class="row">
                        <div class="col-lg-6 col-12">
                          <? if ( ! empty( $item['image']['id'] ) ) : ?>
                            <img class="img-fluid" src="<?= wp_get_attachment_image_url( $item['image']['id'], 'large' ) ?>" alt="Image">
                          <? endif; ?>
                        </div>

                        <div class="col-lg-6 col-12">
                          <div class="h-100 d-flex align-items-center">
                            <div>
                              <?= wpautop( $item['content'] ) ?>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                <? endforeach; ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    <?php endif; ?>
    <?php
  }
}
