<?php

defined( 'ABSPATH' ) or exit;

use Elementor\Controls_Manager;
use Elementor\Widget_Base;

class Be_Inspired extends Widget_Base {
  public function get_name() {
    return 'Be_Inspired';
  }

  public function get_title() {
    return 'Be Inspired';
  }

  public function get_icon() {
    return 'fa fa-puzzle-piece';
  }

  public function get_categories() {
    return [ 'custom' ];
  }

  protected function _register_controls() {
    $this->start_controls_section(
      'content_section',
      [
        'label' => 'Content',
        'tab'   => Controls_Manager::TAB_CONTENT,
      ]
    );

    // Add controls here
    $this->add_control(
      'message',
      [
        'label'     => __( 'Show all inspiration posts and categories', 'plugin-name' ),
        'type'      => \Elementor\Controls_Manager::HEADING,
        'separator' => 'after',
      ]
    );

    $this->end_controls_section();
  }

  protected function render() {
    global $post;

    $uid = uniqid( 'be-inspired-' );

    $inspire_terms = get_terms( [
      'taxonomy'   => 'inspire_cat',
      'hide_empty' => true,
    ] );

    $posts_list = [];

    if ( ! is_wp_error( $inspire_terms ) && ! empty( $inspire_terms ) ) : $inspire_terms = array_values( $inspire_terms ); ?>
      <div id="<?= $uid ?>" class="be-inspired">
        <div class="row">
          <div class="col-lg-2 col-12">
            <div class="position-relative">
              <ul class="be-inspired__nav nav nav-tabs" role="tablist">
                <? foreach ( $inspire_terms as $index => $inspire_term ) : ?>
                  <li class="nav-item">
                    <a class="nav-link mb-0 <?= 0 == $index ? 'active' : '' ?>" data-toggle="tab" data-id="<?= $index + 1 ?>" href="#tab-<?= "$uid-{$inspire_term->slug}" ?>" role="tab" aria-controls="tab-<?= "$uid-{$inspire_term->slug}" ?>" aria-selected="true">
                      <?= $inspire_term->name ?>
                    </a>
                  </li>
                <? endforeach; ?>
              </ul>

              <div class="be-inspired__line"><span class="be-inspired__line-inner"></span></div>
            </div>
          </div>

          <div class="col-lg-10 col-12">
            <div class="tab-content">
              <? foreach ( $inspire_terms as $index => $inspire_term ) : ?>
                <div class="tab-pane fade <?= 0 == $index ? 'show active' : '' ?>" id="tab-<?= "$uid-{$inspire_term->slug}" ?>" role="tabpanel">
                  <div class="carousel-items">
                    <?
                    $term_posts = get_posts( [
                      'post_type'      => 'cpt-inspire',
                      'posts_per_page' => 5,
                      'post_status'    => 'publish',
                      'tax_query'      => array(
                        array(
                          'taxonomy' => 'inspire_cat',
                          'field'    => 'slug',
                          'terms'    => $inspire_term->slug,
                        ),
                      ),
                    ] );

                    foreach ( $term_posts as $term_post ): ?>
                      <? if ( has_post_thumbnail( $term_post ) ) : ?>
                        <div class="carousel-item">
                          <?php
                          $post = get_post( $term_post );
                          setup_postdata( $post );

                          $posts_list[] = $post->ID;

                          get_template_part( 'template-parts/loops/inspiration-slide' );
                          ?>
                        </div>
                      <? endif; ?>
                    <?php endforeach; ?>
                    <? wp_reset_postdata(); ?>
                  </div>
                </div>
              <? endforeach; ?>
            </div>
          </div>
        </div>

        <?
        if ( ! empty( $posts_list ) ) {
          foreach ( $posts_list as $post_id ) {
            $post = get_post( $post_id );
            setup_postdata( $post );
            get_template_part( 'template-parts/modals/be-inspired' );
          }

          wp_reset_postdata();
        }
        ?>
      </div>
    <?php endif; ?>
    <?php
  }
}
