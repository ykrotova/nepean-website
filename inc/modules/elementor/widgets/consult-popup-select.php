<?php

defined( 'ABSPATH' ) or exit;

use Elementor\Controls_Manager;
use Elementor\Widget_Base;

class Consult_Popup_Select extends Widget_Base {
  public function get_name() {
    return 'Consult_Popup_Select';
  }

  public function get_title() {
    return 'Consult Popup Select';
  }

  public function get_icon() {
    return 'fa fa-puzzle-piece';
  }

  public function get_categories() {
    return [ 'custom' ];
  }

  protected function _register_controls() {
    $this->start_controls_section(
      'content_section',
      [
        'label' => 'Content',
        'tab'   => Controls_Manager::TAB_CONTENT,
      ]
    );

    // Add controls here
    $this->add_control(
      'message',
      [
        'label' => __( 'Just show a dropdown of consultation types', 'plugin-name' ),
        'type'  => \Elementor\Controls_Manager::HEADING,
      ]
    );

    $this->end_controls_section();
  }

  protected function render() {
    //$settings = $this->get_settings_for_display();

    ?>
    <div class="form-wrap form-wrap--footer">
      <div class="select-wrap bg-white">
        <select id="consult-popup-select" name="placeholder">
          <? foreach ( CONSULTATION_TYPES as $key => $consultation_type ) : ?>
            <option value="<?= esc_attr( $key ) ?>"><?= $consultation_type ?></option>
          <? endforeach; ?>
        </select>
      </div>
    </div>
    <?php
  }
}
