<?php

defined( 'ABSPATH' ) or exit;

use Elementor\Controls_Manager;
use Elementor\Widget_Base;

class Custom_Spare_Parts_Form extends Widget_Base {
  public function get_name() {
    return 'Custom_Spare_Parts_Form';
  }

  public function get_title() {
    return 'Spare Parts Popup Form';
  }

  public function get_icon() {
    return 'fa fa-puzzle-piece';
  }

  public function get_categories() {
    return [ 'custom' ];
  }

  protected function _register_controls() {
    $this->start_controls_section(
      'content_section',
      [
        'label' => 'Content',
        'tab'   => Controls_Manager::TAB_CONTENT,
      ]
    );

    // Add controls here

    $this->end_controls_section();
  }

  protected function render() {
    $settings = $this->get_settings_for_display();
    ?>

    <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#spare-parts-form">
      Launch demo modal
    </button> -->

    <!-- Modal -->
    <div class="modal fade" id="spare-parts-form" tabindex="-1" role="dialog" aria-labelledby="spare-parts-form" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <?php //echo do_shortcode('');?>
                </div>
            </div>
        </div>
    </div>
    <?php
  }
}
