<?php

defined( 'ABSPATH' ) or exit;

use Elementor\Controls_Manager;
use Elementor\Widget_Base;

class Custom_El_Slider extends Widget_Base {
  public function get_name() {
    return 'Custom_El_Slider';
  }

  public function get_title() {
    return 'Showroom Slider';
  }

  public function get_icon() {
    return 'fa fa-puzzle-piece';
  }

  public function get_categories() {
    return [ 'custom' ];
  }

  protected function _register_controls() {
    $this->start_controls_section(
      'content_section',
      [
        'label' => 'Content',
        'tab'   => Controls_Manager::TAB_CONTENT,
      ]
    );

    // Add controls here
    $this->add_control(
      'message',
      [
        'label'     => __( 'Showroom Slider', 'plugin-name' ),
        'type'      => \Elementor\Controls_Manager::HEADING,
        'separator' => 'after',
      ]
    );

    $this->end_controls_section();
  }

  protected function render() {
    $uid = uniqid( 'gallery-' );

    if ( ! empty( $gallery = get_field( 'gallery' ) ) ) : ?>
      <div id="<?= $uid ?>">
        <div id="slider-<?= $uid ?>" class="carousel carousel--showroom slide" data-ride="carousel">
          <ol class="carousel-indicators">
            <? foreach ( $gallery as $index => $item ) : ?>
              <li data-target="#slider-<?= $uid ?>" data-slide-to="<?= $index ?>" class="<?= 0 == $index ? 'active' : '' ?>"></li>
            <? endforeach; ?>
          </ol>

          <div class="carousel-inner">
            <? foreach ( $gallery as $index => $item ) : ?>
              <div class="carousel-item bg-center <?= 0 == $index ? 'active' : '' ?>" style="background-image: url('<?= $item['gallery_image']['url'] ?>');"></div>
            <? endforeach; ?>
          </div>
        </div>
        <script>
          jQuery(document).ready(function($) {
            $('#<?= $uid ?> .carousel').carousel();
          });
        </script>
      </div>
    <?php endif;
  }
}
