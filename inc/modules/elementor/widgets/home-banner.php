<?php

defined( 'ABSPATH' ) or exit;

use Elementor\Controls_Manager;
use Elementor\Repeater;
use Elementor\Utils;
use Elementor\Widget_Base;

class Home_Banner extends Widget_Base {
  public function get_name() {
    return 'Home_Banner';
  }

  public function get_title() {
    return 'Banner';
  }

  public function get_icon() {
    return 'fa fa-puzzle-piece';
  }

  public function get_categories() {
    return [ 'custom' ];
  }

  protected function _register_controls() {
    $this->start_controls_section(
      'content_section',
      [
        'label' => 'Content',
        'tab'   => Controls_Manager::TAB_CONTENT,
      ]
    );

    // Add controls here
    $repeater = new Repeater();

    $repeater->add_control(
      'image',
      [
        'label'    => __( 'Image', 'wynstan' ),
        'type'     => Controls_Manager::MEDIA,
        'default'  => [
          'url' => Utils::get_placeholder_image_src(),
        ],
        'required' => true,
      ]
    );

    $repeater->add_control(
      'content',
      [
        'label'      => __( 'Content', 'wynstan' ),
        'type'       => Controls_Manager::WYSIWYG,
        'show_label' => false,
      ]
    );

    $repeater->add_control(
      'link_label',
      [
        'label'   => __( 'CTA label', 'wynstan' ),
        'type'    => Controls_Manager::TEXT,
        'default' => 'Find out more',
      ]
    );

    $repeater->add_control(
      'cta_link',
      [
        'label'         => __( 'CTA link', '' ),
        'type'          => Controls_Manager::URL,
        'show_external' => true,
        'default'       => [
          'url'         => '',
          'is_external' => false,
          'nofollow'    => false,
        ],
      ]
    );

    $this->add_control(
      'list',
      [
        'label'  => __( 'Sliders', 'wynstan' ),
        'type'   => Controls_Manager::REPEATER,
        'fields' => $repeater->get_controls(),
      ]
    );

    $this->end_controls_section();
  }

  protected function render() {
    $list = $this->get_settings_for_display( 'list' );

    $uid = uniqid( 'home-banner-' );

    if ( ! empty( $list ) ) : ?>
      <div class="home-slider" id="<?= $uid ?>">
        <div class="container">
          <div class="position-relative home-slider__slide-wrapper">
            <? // Slick slider line indicator ?>
            <div class="home-slider__line"><span class="home-slider__line-inner"></span></div>

            <div class="home-slider__text pl-lg-5 ml-lg-5 carousel">
              <? foreach ( $list as $index => $item ) : ?>
                <div class="home-slider__text-item p-1 carousel-item <?= 0 == $index ? 'active' : '' ?>">
                  <div class="home-slider__text-inner d-flex align-items-center">
                    <div>
                      <?= wpautop( $item['content'] ) ?>

                      <? if ( ! empty( $item['cta_link']['url'] ) ) : ?>
                        <?php get_template_part( 'template-parts/content-button', null, [
                          'link'  => esc_url( $item['cta_link']['url'] ),
                          'text'  => $item['link_label'],
                          'class' => 'my-5',
                        ] ); ?>
                      <? endif; ?>

                      <div class="pt-5">
                        <a class="btn--more-link" data-slide="<?= ( $index + 1 >= count( $list ) ) ? 1 : ( $index + 2 ) ?>" href="#">More</a>
                      </div>
                    </div>
                  </div>
                </div>
              <? endforeach; ?>
            </div>
          </div>
        </div>

        <div class="home-slider__image carousel">
          <? foreach ( $list as $index => $item ) : ?>
            <div class="home-slider__bg-wrapper carousel-item <?= 0 == $index ? 'active' : '' ?>">
              <? if ( ! empty( $item['image']['id'] ) ) : ?>
                <div class="home-slider__bg bg-center" style="background-image:url('<?= wp_get_attachment_image_url( $item['image']['id'], 'full' ) ?>')"></div>
              <? endif; ?>
            </div>
          <? endforeach; ?>
        </div>
      </div>
    <?php endif; ?>
    <?php
  }
}
