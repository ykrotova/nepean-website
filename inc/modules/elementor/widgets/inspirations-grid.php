<?php

defined( 'ABSPATH' ) or exit;

use Elementor\Controls_Manager;
use Elementor\Widget_Base;

class Custom_El_Inspirations_Grid extends Widget_Base {
  public function get_name() {
    return 'Custom_El_Inspirations_Grid';
  }

  public function get_title() {
    return 'Inspirations';
  }

  public function get_icon() {
    return 'fa fa-puzzle-piece';
  }

  public function get_categories() {
    return [ 'custom' ];
  }

  protected function _register_controls() {
    $this->start_controls_section(
      'content_section',
      [
        'label' => 'Content',
        'tab'   => Controls_Manager::TAB_CONTENT,
      ]
    );

    $this->add_control(
      'message',
      [
        'label' => __( 'Used on inspiration term pages only', 'plugin-name' ),
        'type'  => \Elementor\Controls_Manager::HEADING,
      ]
    );

    $this->end_controls_section();
  }

  protected function render() {
    // $settings = $this->get_settings_for_display();

    if ( ! is_archive() && ! is_tax( 'inspire_cat' ) ) {
      return;
    }

    $uid = uniqid( 'inspirations-' );

    $categories = get_terms( [ 'hide_empty' => true, 'taxonomy' => 'inspire_cat' ] );

    ( is_tax( 'inspire_cat' ) ) ? ( $category_select = ( get_queried_object() )->slug ) : ( $category_select = "all" );
    ?>
    <div id="<?= $uid ?>" class="inspirations__wrapper">
      <? if ( ! is_wp_error( $categories ) && ! empty( $categories ) ) : ?>
        <div class="inspirations__filter-bar bg-pink py-3">
          <div class="container">
            <div class="d-flex align-items-center justify-content-center">
              <h4>Inspire me by</h4>
              <form action="#" class="mx-3">
                <label class="select-wrap d-block">
                  <select class="inspirations__select">
                    <option value="<?= get_post_type_archive_link( 'cpt-inspire' ) ?>">All</option>

                    <? foreach ( $categories as $category ) : ?>
                      <option value="<?= get_term_link( $category, 'inspire_cat' ) ?>" <? selected( $category_select == $category->slug ) ?>><?= $category->name ?></option>
                    <? endforeach; ?>
                  </select>
                </label>
              </form>
            </div>
          </div>
        </div>
      <? endif; ?>

      <div class="inspirations__posts py-4 py-xl-5">
        <div class="container-fluid" style="max-width: 1600px;">
          <div class="grid">
            <div class="grid-sizer"></div>
            <?
            ////////////////////////////////////////////////////////////////
            // Infinite ajax load more posts
            $args = [
              'container_type'               => 'div',
              'css_classes'                  => 'inspirations__posts',
              'post_type'                    => 'cpt-inspire',
              'posts_per_page'               => 8,
              'transition'                   => 'masonry',
              'masonry_selector'             => '.grid-item',
              'transition_container_classes' => 'row',
              'offset'                       => 0,
              'images_loaded'                => 'true',
              'max_pages'                    => 1,
              'button_label'                 => 'Load More',
              'taxonomy_operator'            => 'IN',
              'archive'                      => 'true',
              'loading_style'                => 'white',
            ];

            // Combine args
            $combined_args = [];
            foreach ( $args as $key => $value ) {
              $combined_args[] = $key . '="' . $value . '"';
            }
            ?>

            <?= do_shortcode( '[ajax_load_more ' . implode( ' ', $combined_args ) . ']' ) ?>
          </div>
        </div>
      </div>
    </div>
    <?php
  }
}
