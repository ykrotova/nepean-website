<?php

defined( 'ABSPATH' ) or exit;

use Elementor\Controls_Manager;
use Elementor\Widget_Base;

class Custom_El_Jobs_List extends Widget_Base {
  public function get_name() {
    return 'Custom_El_Jobs_List';
  }

  public function get_title() {
    return 'Jobs List';
  }

  public function get_icon() {
    return 'fa fa-puzzle-piece';
  }

  public function get_categories() {
    return [ 'custom' ];
  }

  protected function _register_controls() {
    $this->start_controls_section(
      'content_section',
      [
        'label' => 'Content',
        'tab'   => Controls_Manager::TAB_CONTENT,
      ]
    );

    $this->add_control(
      'message',
      [
        'label' => __( 'Pull all latest job posts', 'plugin-name' ),
        'type'  => \Elementor\Controls_Manager::HEADING,
      ]
    );

    $this->end_controls_section();
  }

  protected function render() {
    $uid = uniqid( "jobs-list-" );

    $args = array(
      'post_type'      => 'cpt-career',
      'posts_per_page' => - 1,
      'post_status'    => 'publish',
    );

    $resources = new WP_Query( $args );

    if ( $resources->have_posts() ) : ?>
      <div class="jobs-list" id="<?= esc_attr( $uid ) ?>">
        <? while ( $resources->have_posts() ) : $resources->the_post(); ?>
          <div class="jobs-list__item p-3 p-xl-4">
            <div class="row">
              <div class="col-lg-8">
                <h6 class="mb-lg-0"><a href="<? the_permalink(); ?>" class="black"><? the_title() ?></a></h6>
              </div>

              <div class="col-lg-4 text-lg-right">
                <a href="<? the_permalink(); ?>" class="btn--arrow-link">Find out more</a>
              </div>
            </div>
          </div>
        <? endwhile; ?>
      </div>
    <? else: ?>
      <p class="py-5 text-center">No jobs found!</p>
    <? endif;

    wp_reset_postdata();
  }
}
