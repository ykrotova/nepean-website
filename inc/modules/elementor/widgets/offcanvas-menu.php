<?php

defined( 'ABSPATH' ) or exit;

use Elementor\Controls_Manager;
use Elementor\Widget_Base;

class Custom_El_Offcanvas_Menu extends Widget_Base {
  public function get_name() {
    return 'Custom_El_Offcanvas_Menu';
  }

  public function get_title() {
    return 'Offcanvas Menu';
  }

  public function get_icon() {
    return 'fa fa-puzzle-piece';
  }

  public function get_categories() {
    return [ 'custom' ];
  }

  protected function _register_controls() {
    $this->start_controls_section(
      'content_section',
      [
        'label' => 'Content',
        'tab'   => Controls_Manager::TAB_CONTENT,
      ]
    );

    // Add controls here

    $this->end_controls_section();
  }

  protected function render() {
    $settings = $this->get_settings_for_display();
    ?>
    <div class="offcanvas" id="offcanvas-menu">
        <div class="h-100 d-flex justify-content-center">
            <div class="w-100">
                <div class="py-3 d-flex align-items-center justify-content-end">
                    <a href="#" class="elementor-item mr-3">
                        <img src="<?= get_assets_path( 'images/icon-search-pink.svg' );?>" alt="">
                    </a>
                    <div class="woo-cart__toggle">
                        <a href="https://local.wynstan.com.au/cart/" class="elementor-item">Cart<span class="woo-cart__count">110</span></a>
                    </div>
                </div>

                <div class="px-5">
                    <nav class="pb-lg-5">
                        <?php if( has_nav_menu('offcanvas_navigation') ):?>
                           <?php echo wp_nav_menu(['theme_location' => 'offcanvas_navigation', 'menu_class' => 'nav']);?>
                        <?php endif;?>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <?php
  }
}
