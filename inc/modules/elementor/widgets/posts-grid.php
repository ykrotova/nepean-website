<?php

defined( 'ABSPATH' ) or exit;

use Elementor\Controls_Manager;
use Elementor\Widget_Base;

class Custom_El_Posts_Grid extends Widget_Base {
  public function get_name() {
    return 'Custom_El_Posts_Grid';
  }

  public function get_title() {
    return 'Posts Grid';
  }

  public function get_icon() {
    return 'fa fa-puzzle-piece';
  }

  public function get_categories() {
    return [ 'custom' ];
  }

  protected function _register_controls() {
    $this->start_controls_section(
      'content_section',
      [
        'label' => 'Content',
        'tab'   => Controls_Manager::TAB_CONTENT,
      ]
    );

    $this->add_control(
      'type',
      [
        'label'     => __( 'Grid Type', 'plugin-domain' ),
        'type'      => Controls_Manager::SELECT,
        'options'   => [
          'archive' => 'Archive',
          'latest'  => 'Latest Posts',
          'related' => 'Related Posts',
        ],
        'default'   => 'archive',
        'separator' => 'after',
      ]
    );

    $this->add_control(
      'message',
      [
        'label'     => __( 'This type\'s settings are programmatically managed', 'plugin-name' ),
        'type'      => \Elementor\Controls_Manager::HEADING,
        'condition' => [
          'type' => [ 'archive' ],
        ],
      ]
    );

    $this->add_control(
      'post_type',
      [
        'label'     => __( 'Post Type', 'plugin-domain' ),
        'type'      => Controls_Manager::SELECT,
        'options'   => [
          'post' => 'Post',
        ],
        'default'   => 'post',
        'condition' => [
          'type' => [ 'latest', 'related' ],
        ],
      ]
    );

    $this->add_control(
      'columns',
      [
        'label'     => __( 'Columns', 'plugin-domain' ),
        'type'      => Controls_Manager::NUMBER,
        'min'       => 1,
        'max'       => 12,
        'step'      => 1,
        'default'   => 3,
        'condition' => [
          'type' => [ 'latest', 'related' ],
        ],
      ]
    );

    $this->add_control(
      'rows',
      [
        'label'     => __( 'Rows', 'plugin-domain' ),
        'type'      => Controls_Manager::NUMBER,
        'min'       => 1,
        'max'       => 50,
        'step'      => 1,
        'default'   => 4,
        'condition' => [
          'type' => [ 'latest', 'related' ],
        ],
      ]
    );

    $this->add_control(
      'latest_tax',
      [
        'label'       => __( 'Term slug', '' ),
        'type'        => Controls_Manager::TEXT,
        'default'     => __( '', 'plugin-domain' ),
        'placeholder' => __( 'E.g. news', 'plugin-domain' ),
        'condition'   => [
          'type' => [ 'latest' ],
        ],
      ]
    );

    $this->end_controls_section();
  }

  protected function render() {
    global $post, $wp_query;

    $type       = $this->get_settings_for_display( 'type' ) ?? 'archive';
    $post_type  = $this->get_settings_for_display( 'post_type' ) ?? 'post';
    $rows       = $this->get_settings_for_display( 'rows' ) ?? 3;
    $columns    = $this->get_settings_for_display( 'columns' ) ?? 3;
    $latest_tax = $this->get_settings_for_display( 'latest_tax' ) ?? '';

    $uid                    = uniqid( "posts-grid-{$type}-" );
    $current_queried_object = null;
    $resources              = $wp_query;
    $map_taxonomies         = [
      'post' => 'category',
    ];

    switch ( $type ) {
      case 'archive':
        $queried_object = get_queried_object();

        if ( ! empty( $queried_object->taxonomy ) && ! empty( $queried_object->slug ) ) {
          $current_queried_object = clone $queried_object;
        }
        break;

      case 'latest':
        $latest_args = array(
          'post_type'      => $post_type,
          'posts_per_page' => $rows * $columns,
          'post_status'    => 'publish',
          'tax_query'      => array(
            array(
              'taxonomy' => $map_taxonomies[ $post_type ],
              'field'    => 'slug',
              'terms'    => [ $latest_tax ],
            ),
          ),
        );

        $resources = new WP_Query( $latest_args );
        break;

      case 'related':
        $terms = get_the_terms( $post->ID, $map_taxonomies[ $post_type ] );

        if ( empty( $terms ) ) {
          $terms = array();
        }

        $term_list = wp_list_pluck( $terms, 'slug' );

        $related_args = array(
          'post_type'      => $post_type,
          'posts_per_page' => $rows * $columns,
          'post_status'    => 'publish',
          'post__not_in'   => array( $post->ID ),
          'tax_query'      => array(
            array(
              'taxonomy' => $map_taxonomies[ $post_type ],
              'field'    => 'slug',
              'terms'    => $term_list,
            ),
          ),
        );

        $resources = new WP_Query( $related_args );
        break;
    }

    if ( $resources->have_posts() ) : ?>
      <div class="posts-grid posts-grid--<?= esc_attr( $type ) ?>" id="<?= esc_attr( $uid ) ?>">
        <div class="row">
          <div class="col-xl-12">
            <div class="row">
              <? while ( $resources->have_posts() ) : $resources->the_post(); ?>
                <div class="col-sm-6 col-xl-<?= absint( ceil( 12 / $columns ) ) ?> mb-4 mb-xl-5">
                  <? get_template_part( 'template-parts/loops/post' ); ?>
                </div>
              <? endwhile; ?>
            </div>

            <? if ( 'archive' == $type ) : ?>
              <div class="text-center pt-3">
                <? wp_pagenavi( array( 'query' => $resources ) ); ?>
              </div>
            <? endif; ?>
          </div>
        </div>
      </div>
    <? else: ?>
      <p class="py-5 text-center">No posts found!</p>
    <? endif;

    wp_reset_postdata();
  }
}
