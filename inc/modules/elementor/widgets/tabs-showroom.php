<?php

defined( 'ABSPATH' ) or exit;

use Elementor\Controls_Manager;
use Elementor\Widget_Base;

class Custom_El_Tabs_Showroom extends Widget_Base {
  public function get_name() {
    return 'Custom_El_Tabs_Showroom';
  }

  public function get_title() {
    return 'Tabs | Showrooms';
  }

  public function get_icon() {
    return 'fa fa-puzzle-piece';
  }

  public function get_categories() {
    return [ 'custom' ];
  }

  protected function _register_controls() {
    $this->start_controls_section(
      'content_section',
      [
        'label' => 'Content',
        'tab'   => Controls_Manager::TAB_CONTENT,
      ]
    );

    // Add controls here
    $this->add_control(
      'message',
      [
        'label'     => __( 'Show store tabs', 'plugin-name' ),
        'type'      => \Elementor\Controls_Manager::HEADING,
        'separator' => 'after',
      ]
    );

    $this->end_controls_section();
  }

  protected function render() {
    // $settings = $this->get_settings_for_display();

    $tabs = [];

    $tabs['contact-details'] = [
      'label'   => 'Contact Details',
      'content' => call_user_func( function () {
        ob_start();

        the_content();
        ?>
        <p class="mt-5">Book a free in-home consultation today!</p>
        <p><a href="#" class="btn" data-toggle="modal" data-target="#modal-free-consultant">Book now</a></p>
        <?php
        return ob_get_clean();
      } ),
    ];

    if ( ! empty( $serviced_areas = get_field( 'serviced_areas' ) ) ) {
      $tabs['serviced-areas'] = [
        'label'   => 'Serviced Areas',
        'content' => wpautop( $serviced_areas ) . do_shortcode( '[wpsl_map]' ),
      ];
    }

    if ( ! empty( $display_products = get_field( 'display_products' ) ) ) {
      $tabs['display_products'] = [
        'label'   => 'Display Products',
        'content' => wpautop( $display_products ),
      ];
    }

    if ( ! empty( $tabs ) ) : ?>
      <?= woo_tabs_html( $tabs ); ?>
    <?php endif; ?>
    <?php
  }
}
