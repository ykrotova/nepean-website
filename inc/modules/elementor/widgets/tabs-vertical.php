<?php

defined( 'ABSPATH' ) or exit;

use Elementor\Controls_Manager;
use Elementor\Repeater;
use Elementor\Widget_Base;

class Custom_El_Tabs_Vertical extends Widget_Base {
  public function get_name() {
    return 'Custom_El_Tabs_Vertical';
  }

  public function get_title() {
    return 'Tabs | Vertical';
  }

  public function get_icon() {
    return 'fa fa-puzzle-piece';
  }

  public function get_categories() {
    return [ 'custom' ];
  }

  protected function _register_controls() {
    $this->start_controls_section(
      'content_section',
      [
        'label' => 'Content',
        'tab'   => Controls_Manager::TAB_CONTENT,
      ]
    );

    // Add controls here
    $this->add_control(
      'message',
      [
        'label'     => __( 'Mixed of inspiration and blog posts', 'plugin-name' ),
        'type'      => \Elementor\Controls_Manager::HEADING,
        'separator' => 'after',
      ]
    );

    $repeater = new Repeater();

    $repeater->add_control(
      'tab_label',
      [
        'label' => __( 'Tab Label', 'wynstan' ),
        'type'  => Controls_Manager::TEXT,
      ]
    );

    $repeater->add_control(
      'items',
      [
        'label'    => __( 'Inspiration/Post', 'plugin-domain' ),
        'type'     => \Elementor\Controls_Manager::SELECT2,
        'multiple' => true,
        'options'  => call_user_func( function () {
          $data = [];

          $posts = get_posts( [
            'post_type'      => 'post',
            'posts_per_page' => - 1,
            'post_status'    => 'publish',
          ] );

          $inspirations = get_posts( [
            'post_type'      => 'cpt-inspire',
            'posts_per_page' => - 1,
            'post_status'    => 'publish',
          ] );

          foreach ( array_merge( $posts, $inspirations ) as $p ) {
            $data[ $p->ID ] = ucwords( str_replace( [ 'cpt-' ], [ '' ], $p->post_type ) ) . ' | ' . $p->post_title;
          }

          return $data;
        } ),
      ]
    );

    $this->add_control(
      'list',
      [
        'label'       => __( 'Tabs', 'wynstan' ),
        'type'        => Controls_Manager::REPEATER,
        'fields'      => $repeater->get_controls(),
        'title_field' => '{{{ tab_label }}}',
      ]
    );

    $this->end_controls_section();
  }

  protected function render() {
    global $post;

    $uid = uniqid( 'tabs-vertical-' );

    $list = $this->get_settings_for_display( 'list' );

    $inspire_list = [];

    if ( ! function_exists( 'tabs_vertical_cmp' ) ) {
      function tabs_vertical_cmp( $a, $b ) {
        return ( get_post_field( 'menu_order', $a ) < get_post_field( 'menu_order', $b ) ) ? - 1 : 1;
      }
    }

    if ( ! empty( $list ) ) : ?>
      <div id="<?= $uid ?>" class="be-inspired">
        <div class="row">
          <div class="col-lg-2 col-12">
            <div class="position-relative">
              <ul class="be-inspired__nav nav nav-tabs" role="tablist">
                <? foreach ( $list as $index => $item ) : ?>
                  <li class="nav-item">
                    <a class="nav-link mb-0 <?= 0 == $index ? 'active' : '' ?>" data-toggle="tab" data-id="<?= $index + 1 ?>" href="#tab-<?= "$uid-" . sanitize_key( $item['tab_label'] ) ?>" role="tab" aria-controls="tab-<?= "$uid-" . sanitize_key( $item['tab_label'] ) ?>" aria-selected="true">
                      <?= $item['tab_label'] ?>
                    </a>
                  </li>
                <? endforeach; ?>
              </ul>

              <div class="be-inspired__line"><span class="be-inspired__line-inner"></span></div>
            </div>
          </div>

          <div class="col-lg-10 col-12">
            <div class="tab-content">
              <? foreach ( $list as $index => $item ) : ?>
                <div class="tab-pane fade <?= 0 == $index ? 'show active' : '' ?>" id="tab-<?= "$uid-" . sanitize_key( $item['tab_label'] ) ?>" role="tabpanel">
                  <div class="carousel-items">
                    <?
                    usort( $item['items'], 'tabs_vertical_cmp' );

                    foreach ( $item['items'] as $p ): ?>
                      <? if ( has_post_thumbnail( $p ) ) : ?>
                        <div class="carousel-item">
                          <?php
                          $post = get_post( $p );
                          setup_postdata( $post );

                          if ( 'cpt-inspire' == $post->post_type ) {
                            $inspire_list[] = $post->ID;
                            get_template_part( 'template-parts/loops/inspiration-slide' );
                          } else {
                            get_template_part( 'template-parts/loops/post-slide' );
                          }

                          ?>
                        </div>
                      <? endif; ?>
                    <?php endforeach; ?>
                    <? wp_reset_postdata(); ?>
                  </div>
                </div>
              <? endforeach; ?>
            </div>
          </div>
        </div>

        <?
        if ( ! empty( $inspire_list ) ) {
          foreach ( $inspire_list as $post_id ) {
            $post = get_post( $post_id );
            setup_postdata( $post );
            get_template_part( 'template-parts/modals/be-inspired' );
          }

          wp_reset_postdata();
        }
        ?>
      </div>
    <?php endif; ?>
    <?php
  }
}
