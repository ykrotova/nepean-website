<?php

defined( 'ABSPATH' ) or exit;

use Elementor\Controls_Manager;
use Elementor\Repeater;
use Elementor\Utils;
use Elementor\Widget_Base;

class Custom_El_Tabs extends Widget_Base {
  public function get_name() {
    return 'Custom_El_Tabs';
  }

  public function get_title() {
    return 'Tabs';
  }

  public function get_icon() {
    return 'fa fa-puzzle-piece';
  }

  public function get_categories() {
    return [ 'custom' ];
  }

  protected function _register_controls() {
    $this->start_controls_section(
      'content_section',
      [
        'label' => 'Content',
        'tab'   => Controls_Manager::TAB_CONTENT,
      ]
    );

    // Add controls here
    $repeater = new Repeater();

    $repeater->add_control(
      'image',
      [
        'label'    => __( 'Icon', 'wynstan' ),
        'type'     => Controls_Manager::MEDIA,
        'default'  => [
          'url' => Utils::get_placeholder_image_src(),
        ],
        'required' => true,
      ]
    );

    $repeater->add_control(
      'content',
      [
        'label'      => __( 'Content', 'wynstan' ),
        'type'       => Controls_Manager::WYSIWYG,
        'show_label' => false,
      ]
    );

    $this->add_control(
      'list',
      [
        'label'  => __( 'Tabs', 'wynstan' ),
        'type'   => Controls_Manager::REPEATER,
        'fields' => $repeater->get_controls(),
      ]
    );

    $this->end_controls_section();
  }

  protected function render() {
    $list = $this->get_settings_for_display( 'list' );

    $uid = uniqid( 'tabs-' );

    if ( ! empty( $list ) ) : ?>
      <div id="<?= $uid ?>">
        <div class="steps">
          <div class="block-nav-overflow">
            <ul class="nav nav-pills mb-3 justify-content-lg-around" id="<?= $uid ?>-pills-tab" role="tablist">
              <? foreach ( $list as $index => $item ) : ?>
                <li class="nav-item">
                  <a class="nav-link <?= 0 == $index ? 'active' : '' ?>" id="<?= $uid ?>-pill-tab-<?= $index ?>" data-toggle="pill" href="#<?= $uid ?>-pill-tab-content-<?= $index ?>" role="tab" aria-controls="<?= $uid ?>-pill-tab-content-<?= $index ?>" aria-selected="true">
                    <? if ( ! empty( $item['image']['id'] ) ) : ?>
                      <div class="img-wrap">
                        <img src="<?= wp_get_attachment_image_url( $item['image']['id'], 'large' ) ?>" alt="Icon"/>
                      </div>
                    <? endif; ?>

                    <h3 class="step">Step <?= ( $index + 1 ) ?></h3>
                  </a>
                </li>
              <? endforeach; ?>
            </ul>
          </div>

          <div class="tab-content" id="<?= $uid ?>-pills-tabContent">
            <? foreach ( $list as $index => $item ) : ?>
              <div class="tab-pane fade <?= 0 == $index ? 'show active' : '' ?>" id="<?= $uid ?>-pill-tab-content-<?= $index ?>" role="tabpanel" aria-labelledby="<?= $uid ?>-pill-tab-<?= $index ?>">
                <?= wpautop( $item['content'] ) ?>
              </div>
            <? endforeach; ?>
          </div>
        </div>
      </div>
    <?php endif; ?>
    <?php
  }
}
