<?php

defined( 'ABSPATH' ) or exit;

use Elementor\Controls_Manager;
use Elementor\Widget_Base;

class Custom_El_May_Also_like extends Widget_Base {
  public function get_name() {
    return 'Custom_El_May_Also_like';
  }

  public function get_title() {
    return 'You May Also Like';
  }

  public function get_icon() {
    return 'fa fa-puzzle-piece';
  }

  public function get_categories() {
    return [ 'custom' ];
  }

  protected function _register_controls() {
    $this->start_controls_section(
      'content_section',
      [
        'label' => 'Content',
        'tab'   => Controls_Manager::TAB_CONTENT,
      ]
    );

    // Add controls here

    $this->end_controls_section();
  }

  protected function render() {
    $settings = $this->get_settings_for_display();

    $uid = uniqid( 'may_like-' );
    ?>
    <div id="<?= $uid ?>">
    <div class="may-also-like">
    <h2>You may also like</h2>
    <div class="container ymal">

        <div class="row">
            <div class="col-12 col-lg-4 col-md-4 col-sm-12 left-three">
                <div class="cont block">
                    <div class="img-bl">
                    <img src ="/wp-content/themes/wynstan-2021/assets/images/Roman-blinds.png"/>
                    </div>
                    <div class="title-bl">
                        <h3><a href="#">Roman blinds</a></h3>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4 col-md-4 col-sm-12 mid-three">
                <div class="img-bl">
                <img src ="/wp-content/themes/wynstan-2021/assets/images/glide-blinds.png"/>
                </div>
                <div class="title-bl">
                    <h3><a href="#">Panel glide blinds</a></h3>
                </div>
            </div>
            <div class="col-12 col-lg-4 col-md-4 col-sm-12 mid-right">
                <div class="img-bl">
                <img src ="/wp-content/themes/wynstan-2021/assets/images/cellular-blinds.png"/>
                </div>
                <div class="title-bl">
                <h3><a href="#">Whisper cellular blinds</a></h3>
                </div>
            </div>
        </div>
    </div>
</div>
  </div>
    <?php
  }
}
