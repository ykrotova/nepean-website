<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 */

defined( 'ASSETS_VERSION' ) or define( 'ASSETS_VERSION', md5( filemtime( get_theme_file_path( 'dist/main.min.css' ) ) . filemtime( get_theme_file_path( 'dist/main.min.js' ) ) ) );
defined( 'TERM_SPARE_PARTS' ) or define( 'TERM_SPARE_PARTS', 'pa_spare-parts' );
defined( 'TERM_FABRIC' ) or define( 'TERM_FABRIC', 'pa_fabric-product' );
defined( 'TERM_OPACITY' ) or define( 'TERM_OPACITY', 'pa_opacity' );
defined( 'TERM_COLOUR' ) or define( 'TERM_COLOUR', 'pa_colour' );
defined( 'TERM_COLOUR_SCHEME' ) or define( 'TERM_COLOUR_SCHEME', 'pa_colour-scheme' );
defined( 'KEY_VAR_COLOR_SCHEME' ) or define( 'KEY_VAR_COLOR_SCHEME', 'variation_colour_scheme' );
defined( 'CAT_FABRIC' ) or define( 'CAT_FABRIC', 'fabric-samples' );
defined( 'CAT_SPARE_PARTS' ) or define( 'CAT_SPARE_PARTS', 'spare-parts' );
defined( 'CAT_NORMAL_PRODUCTS' ) or define( 'CAT_NORMAL_PRODUCTS', 'products' );
defined( 'MAX_SAMPLES_PER_PRODUCT' ) or define( 'MAX_SAMPLES_PER_PRODUCT', 5 );
defined( 'MAX_SAMPLES_ALL_PRODUCT' ) or define( 'MAX_SAMPLES_ALL_PRODUCT', 25 );
defined( 'CLASSES_PRODUCT_LOOP_SPACING' ) or define( 'CLASSES_PRODUCT_LOOP_SPACING', 'px-2 px-lg-3 px-xl-4' );
defined( 'CONSULTATION_TYPES' ) or define( 'CONSULTATION_TYPES', array(
  'consultation-type'         => 'Consultation Type',
  'free-in-home-consultation' => "FREE In-home Consultation",
  'free-video-consultation'   => "FREE Video Consultation",
  'free-diy-consultation'     => "FREE DIY Consultation",
) );
define( 'WPSL_MARKER_URI', dirname( get_bloginfo( 'stylesheet_url' ) ) . '/assets/images/markers/' );
defined( 'WOO_GROUP_ITEMS' ) or define( 'WOO_GROUP_ITEMS', [ // Predefined groups for grouping products in cart/order
  'item_group_fabric'         => [],
  'item_group_wynstan_health' => [],
  'item_group_spare_parts'    => [],
  'item_group_others'         => [],
] );

add_filter( 'acf/settings/show_admin', ( defined( 'SHOW_ACF' ) && SHOW_ACF ? '__return_true' : '__return_false' ) );
add_filter( 'wpsl_skip_cpt_template', '__return_false' );
remove_shortcode( 'wpsl_address' );

add_shortcode( 'wpsl_address', 'custom_wpsl_store_address' );
function custom_wpsl_store_address( $atts ) {
  global $post, $wpsl_settings, $wpsl;

  $atts = wpsl_bool_check( shortcode_atts( apply_filters( 'wpsl_address_shortcode_defaults', array(
    'id'                        => '',
    'name'                      => true,
    'address'                   => true,
    'address2'                  => true,
    'city'                      => true,
    'state'                     => true,
    'zip'                       => true,
    'country'                   => true,
    'phone'                     => true,
    'fax'                       => true,
    'email'                     => true,
    'url'                       => true,
    'directions'                => true,
    'clickable_contact_details' => (bool) $wpsl_settings['clickable_contact_details'],
  ) ), $atts ) );

  if ( get_post_type() == 'wpsl_stores' ) {
    if ( empty( $atts['id'] ) ) {
      if ( isset( $post->ID ) ) {
        $atts['id'] = $post->ID;
      } else {
        return '';
      }
    }
  } elseif ( empty( $atts['id'] ) ) {
    return __( 'If you use the [wpsl_address] shortcode outside a store page you need to set the ID attribute.', 'wpsl' );
  }

  $content = '<div class="wpsl-locations-details">';

  if ( $atts['name'] && $name = get_the_title( $atts['id'] ) ) {
    $content .= '<span><strong>' . esc_html( $name ) . '</strong></span>';
  }

  $content .= '<div class="wpsl-location-address"><span class="wpsl-icon"><img src="' . get_assets_path( 'images/icon-address.svg' ) . '" alt="Address" class="img-fluid" width="13"></span>';

  if ( $atts['address'] && $address = get_post_meta( $atts['id'], 'wpsl_address', true ) ) {
    $content .= '<span>' . esc_html( $address ) . '</span><br/>';
  }

  if ( $atts['address2'] && $address2 = get_post_meta( $atts['id'], 'wpsl_address2', true ) ) {
    $content .= '<span>' . esc_html( $address2 ) . '</span><br/>';
  }

  $address_format = explode( '_', $wpsl_settings['address_format'] );
  $count          = count( $address_format );
  $i              = 1;

  // Loop over the address parts to make sure they are shown in the right order.
  foreach ( $address_format as $address_part ) {
    // Make sure the shortcode attribute is set to true for the $address_part, and it's not the 'comma' part.
    if ( $address_part != 'comma' && $atts[ $address_part ] ) {
      $post_meta = get_post_meta( $atts['id'], 'wpsl_' . $address_part, true );

      if ( $post_meta ) {
        /*
         * Check if the next part of the address is set to 'comma'.
         * If so add the, after the current address part, otherwise just show a space
         */
        if ( isset( $address_format[ $i ] ) && ( $address_format[ $i ] == 'comma' ) ) {
          $punctuation = ', ';
        } else {
          $punctuation = ' ';
        }

        // If we have reached the last item add a <br /> behind it.
        $br = ( $count == $i ) ? '<br />' : '';

        $content .= '<span>' . esc_html( $post_meta ) . $punctuation . '</span>' . $br;
      }
    }

    $i ++;
  }

  if ( $atts['country'] && $country = get_post_meta( $atts['id'], 'wpsl_country', true ) ) {
    $content .= '<span>' . esc_html( $country ) . '</span>';
  }

  if ( $atts['directions'] && $address ) {
    /*if ( $wpsl_settings['new_window'] ) {
      $new_window = ' target="_blank"';
    } else {
      $new_window = '';
    }*/

    $new_window = ' target="_blank"';

    $content .= '<div class="wpsl-location-directions">';

    $city          = get_post_meta( $atts['id'], 'wpsl_city', true );
    $country       = get_post_meta( $atts['id'], 'wpsl_country', true );
    $destination   = $address . ',' . $city . ',' . $country;
    $direction_url = "https://maps.google.com/maps?saddr=&daddr=" . urlencode( $destination ) . "&travelmode=" . strtolower( $GLOBALS['wpsl']->frontend->get_directions_travel_mode() );

    $content .= '<p><a class="font-weight-bold" ' . $new_window . ' href="' . esc_url( $direction_url ) . '">' . __( 'Directions', 'wpsl' ) . '</a></p>';
    $content .= '</div>';
  }

  $content .= '</div>';

  // If either the phone, fax, email or url is set to true, then add the wrap div for the contact details.
  if ( $atts['phone'] || $atts['fax'] || $atts['email'] || $atts['url'] ) {
    $phone = get_post_meta( $atts['id'], 'wpsl_phone', true );
    $fax   = get_post_meta( $atts['id'], 'wpsl_fax', true );
    $email = get_post_meta( $atts['id'], 'wpsl_email', true );

    if ( $atts['clickable_contact_details'] ) {
      $contact_details = array(
        'phone' => '<a href="tel:' . esc_attr( $phone ) . '">' . esc_html( $phone ) . '</a>',
        'fax'   => '<a href="tel:' . esc_attr( $fax ) . '">' . esc_html( $fax ) . '</a>',
        'email' => '<a href="mailto:' . sanitize_email( $email ) . '">' . sanitize_email( $email ) . '</a>',
      );
    } else {
      $contact_details = array(
        'phone' => esc_html( $phone ),
        'fax'   => esc_html( $fax ),
        'email' => sanitize_email( $email ),
      );
    }

    $content .= '<div class="wpsl-contact-details">';

    if ( $atts['phone'] && $phone ) {
      $content .= '<span class="wpsl-icon"><img src="' . get_assets_path( 'images/icon-phone.svg' ) . '" alt="Phone" class="img-fluid" width="13"></span> <span>' . $contact_details['phone'] . '</span><br/>';
    }

    if ( $atts['fax'] && $fax ) {
      $content .= esc_html( $wpsl->i18n->get_translation( 'fax_label', __( 'Fax', 'wpsl' ) ) ) . ': <span>' . $contact_details['fax'] . '</span><br/>';
    }

    if ( $atts['email'] && $email ) {
      $content .= '<span class="wpsl-icon"><img src="' . get_assets_path( 'images/icon-email.svg' ) . '" alt="Email" class="img-fluid" width="13"></span> <span>' . $contact_details['email'] . '</span><br/>';
    }

    if ( $atts['url'] && $store_url = get_post_meta( $atts['id'], 'wpsl_url', true ) ) {
      $new_window = ( $wpsl_settings['new_window'] ) ? 'target="_blank"' : '';
      $content    .= esc_html( $wpsl->i18n->get_translation( 'url_label', __( 'Url', 'wpsl' ) ) ) . ': <a ' . $new_window . ' href="' . esc_url( $store_url ) . '">' . esc_url( $store_url ) . '</a><br/>';
    }

    $content .= '</div>';
  }

  if ( ! empty( $open_hour_text = get_field( 'open_hour_text', $atts['id'] ) ) ) {
    $content .= '<div class="wpsl-location-address wpsl-open-hours"><span class="wpsl-icon"><img src="' . get_assets_path( 'images/icon-clock.svg' ) . '" alt="Clock" class="img-fluid" width="13"></span>';
    $content .= wpautop( $open_hour_text );
    $content .= '</div>';
  }

  $content .= '</div>';

  return $content;
}

add_filter( 'wpsl_templates', __NAMESPACE__ . '\\custom_wpsl_templates' );
function custom_wpsl_templates( $templates ) {
  if ( ! empty( $templates ) ) {
    foreach ( $templates as &$template ) {
      if ( 'default' == $template['id'] ) {
        $template['path'] = locate_template( 'template-parts/custom-wpsl-default.php' );
      }
    }
  }

  return $templates;
}

add_filter( 'wpsl_admin_marker_dir', __NAMESPACE__ . '\\custom_admin_marker_dir' );
function custom_admin_marker_dir() {
  return get_template_directory() . '/assets/images/markers/';
}

add_filter( 'wpsl_post_type_args',
  function ( $args ) {
    $args['capability_type'] = 'post';

    return $args;
  }
);

add_filter( 'wpsl_marker_props',
  function ( $marker_props ) {
    $marker_props['scaledSize'] = '25,25';
    $marker_props['anchor']     = '12.5,25';

    return $marker_props;
  }
);

add_filter( 'body_class', 'body_classes' );
function body_classes( $classes ) {
  if ( ! is_singular() ) {
    $classes[] = 'hfeed';
  }

  if ( woo_has_products_filter_topbar() ) {
    $classes[] = 'archive-with-topbar';
  }

  if ( woo_is_spare_parts_terms() ) {
    $classes[] = 'archive-spare-parts';
  }

  if ( woo_has_products_filter_sidebar() ) {
    $classes[] = 'archive-with-sidebar';
  }

  return $classes;
}

add_action( 'wp_head', 'pingback_header' );
function pingback_header() {
  if ( is_singular() && pings_open() ) {
    echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
  }
}

add_action( 'after_setup_theme', 'setup_theme' );
function setup_theme() {
  add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption', ) );
  add_theme_support( 'automatic-feed-links' );
  add_theme_support( 'customize-selective-refresh-widgets' );

  add_theme_support( 'title-tag' );
  add_theme_support( 'post-thumbnails' );

  add_image_size( 'thumbnails_600_400', 600, 400, true );

  add_post_type_support( 'post', 'excerpt' );
  add_post_type_support( 'page', 'excerpt' );

  register_nav_menus( array(
    'primary_menu'         => __( 'Primary Menu' ),
    'top_menu'             => __( 'Top Menu' ),
    'offcanvas_navigation' => __( 'Offcanvas Navigation' ),
  ) );
}

add_action( 'acf/init', 'add_site_settings_page' );
function add_site_settings_page() {
  if ( ( current_user_can( 'manage_options' ) || current_user_can( 'edit_posts' ) ) && function_exists( 'acf_add_options_page' ) ) {
    acf_add_options_page( array(
      'page_title'    => 'Site Settings',
      'menu_title'    => 'Site Settings',
      'menu_slug'     => 'site-settings',
      'capability'    => 'edit_posts',
      'icon_url'      => 'dashicons-art',
      'update_button' => 'Save Settings',
      'redirect'      => false,
    ) );
  }
}

add_action( 'wp_enqueue_scripts', 'enqueue_scripts', 99 );
function enqueue_scripts() {
  $suffix = SCRIPT_DEBUG ? '' : '.min';

  /* Common assets */
  $assets_css = [
    'css-bootstrap-reboot' => 'plugins/bootstrap-4.5.0/css/bootstrap-reboot.min.css',
    'css-bootstrap-grid'   => 'plugins/bootstrap-4.5.0/css/bootstrap-grid.min.css',
    'css-bootstrap'        => 'plugins/bootstrap-4.5.0/css/bootstrap.min.css',
    'css-font-awesome'     => 'plugins/font-awesome-4.7.0/css/font-awesome.min.css',
  ];
  $assets_js  = [
    'js-bootstrap'   => 'plugins/bootstrap-4.5.0/js/bootstrap.bundle.min.js',
    'js-css-browser' => 'plugins/css_browser_selector/css_browser_selector.js',
    'js-wow'         => 'plugins/wow/dist/wow.min.js',
  ];

  /* Specific assets */
  $assets_css['css-slick']       = 'plugins/slick/slick.min.css';
  $assets_css['css-slick-theme'] = 'plugins/slick/slick-theme.min.css';
  $assets_js['js-slick']         = 'plugins/slick/slick.min.js';

  if ( is_product() ) {
    $assets_js['js-readmore'] = 'plugins/Readmore/readmore.min.js';
  }

  /* Enqueue */
  wp_enqueue_script( 'jquery' );
  wp_enqueue_script( 'underscore' );
  wp_enqueue_script( 'google-recaptcha' );
  foreach ( $assets_css as $handle => $path ) {
    wp_enqueue_style( $handle, get_assets_path( $path ), [], ASSETS_VERSION );
  }
  foreach ( $assets_js as $handle => $path ) {
    wp_enqueue_script( $handle, get_assets_path( $path ), [], ASSETS_VERSION, true );
  }

  /* Conditions */
  wp_script_add_data( 'theme-html5', 'conditional', 'lt IE 9' );
  wp_script_add_data( 'theme-respond', 'conditional', 'lt IE 9' );

  /* Main assets */
  wp_enqueue_style( 'theme-dashicons', includes_url( "css/dashicons$suffix.css" ), [], ASSETS_VERSION );
  wp_enqueue_style( 'theme-style', get_theme_file_uri( 'dist/main.min.css' ), [], ASSETS_VERSION );
  wp_enqueue_style( 'theme-style-custom', get_theme_file_uri( 'assets/css/custom.css' ), [], ASSETS_VERSION );
  wp_enqueue_script( 'theme-js', get_theme_file_uri( 'dist/main.min.js' ), [], ASSETS_VERSION, true );

  wp_localize_script( 'theme-js',
    'Wynstan',
    [
      'ajaxUrl' => admin_url( 'admin-ajax.php' ),
    ]
  );
}

add_action( 'init', 'cpt_register_career' );
function cpt_register_career() {
  $slug      = 'career';
  $post_type = 'cpt-career';
  $public    = true;

  $labels = array(
    'name'               => _x( 'Careers', 'post type general name' ),
    'singular_name'      => _x( 'Career', 'post type singular name' ),
    'add_new'            => _x( 'Add Career', 'rep' ),
    'add_new_item'       => __( 'Add New Career' ),
    'edit_item'          => __( 'Edit Career' ),
    'new_item'           => __( 'New Career' ),
    'view_item'          => __( 'View Career' ),
    'search_items'       => __( 'Search Career' ),
    'not_found'          => __( 'Nothing found' ),
    'not_found_in_trash' => __( 'Nothing found in Trash' ),
    'parent_item_colon'  => '',
  );

  $args = array(
    'labels'             => $labels,
    'public'             => true,
    'publicly_queryable' => $public,
    'show_ui'            => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => $slug, 'with_front' => false ),
    'capability_type'    => 'post',
    'hierarchical'       => true,
    'menu_position'      => null,
    'show_in_rest'       => true,
    'menu_icon'          => 'dashicons-id-alt',
    'supports'           => array( 'title', 'editor', 'excerpt', 'revisions' ),
    'has_archive'        => false,
  );
  register_post_type( $post_type, $args );
}

add_action( 'init', 'cpt_register_inspire' );
function cpt_register_inspire() {
  $slug      = 'inspiration';
  $post_type = 'cpt-inspire';
  $public    = true;

  $labels = array(
    'name'               => _x( 'Inspiration', 'post type general name' ),
    'singular_name'      => _x( 'Inspiration', 'post type singular name' ),
    'add_new'            => _x( 'Add Inspiration', 'rep' ),
    'add_new_item'       => __( 'Add New Inspiration' ),
    'edit_item'          => __( 'Edit Inspiration' ),
    'new_item'           => __( 'New Inspiration' ),
    'view_item'          => __( 'View Inspiration' ),
    'search_items'       => __( 'Search Inspiration' ),
    'not_found'          => __( 'Nothing found' ),
    'not_found_in_trash' => __( 'Nothing found in Trash' ),
    'parent_item_colon'  => '',
  );

  $args = array(
    'labels'             => $labels,
    'public'             => true,
    'publicly_queryable' => $public,
    'show_ui'            => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => $slug, 'with_front' => false ),
    'capability_type'    => 'post',
    'hierarchical'       => true,
    'menu_position'      => null,
    'show_in_rest'       => true,
    'menu_icon'          => 'dashicons-lightbulb',
    'supports'           => array( 'title', 'editor', 'thumbnail', 'revisions' ),
    'has_archive'        => 'inspiration',
  );
  register_post_type( $post_type, $args );

  register_taxonomy(
    'inspire_cat',
    array(
      'cpt-inspire',
    ),
    array(
      'query_var'         => true,
      'hierarchical'      => true,
      'show_ui'           => true,
      'show_admin_column' => true,
      'show_in_rest'      => true,
      'rewrite'           => array( 'slug' => 'inspiration-category', 'with_front' => false ),
      'label'             => __( 'Inspiration Category' ),
    )
  );
}

add_filter( 'wpseo_breadcrumb_links', 'yoast_seo_breadcrumb_append_link' );
function yoast_seo_breadcrumb_append_link( $links ) {
  if ( is_singular( 'cpt-career' ) ) {
    // $page = home_url('about-us/#careers');
    $breadcrumb[] = array(
      'url'  => home_url( 'about-us/#careers' ),
      'text' => 'Careers',
    );
    array_splice( $links, 1, - 2, $breadcrumb );
  } elseif ( is_singular( 'wpsl_stores' ) ) {
    $breadcrumb[] = array(
      'url'  => home_url( 'showrooms' ),
      'text' => 'Showrooms',
    );
    array_splice( $links, 1, - 2, $breadcrumb );
  }

  if ( ! empty( $page ) ) {
    $breadcrumb[] = array(
      'url'  => get_permalink( $page ),
      'text' => get_the_title( $page ),
    );
    array_splice( $links, 1, - 2, $breadcrumb );
  }

  return $links;
}

add_filter( 'excerpt_more', 'custom_excerpt_more' );
function custom_excerpt_more( $more ) {
  return ' ...';
}

add_filter( 'excerpt_length', 'custom_excerpt_length' );
function custom_excerpt_length( $length ) {
  return 25;
}

add_action( 'wp_head', 'hook_head' );
function hook_head() {
  ?>
  <script type="text/javascript">var $ = jQuery.noConflict();</script>
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet">
  <?php
}

add_action( 'wp_footer', 'custom_wp_footer', 99 );
function custom_wp_footer() {
  if ( is_user_logged_in() ) {
    ?>
    <style>
        #wp-admin-bar-elementor_edit_page:hover .ab-sub-wrapper {
            display: block;
        }
    </style>
    <?php
  }

  get_template_part( 'template-parts/modals/free-consultant' );
  get_template_part( 'template-parts/modals/order-fabric' );

  if ( ! is_cart() ) {
    get_template_part( 'template-parts/modals/cart-sidebar' );
  }

  if ( woo_has_products_filter_sidebar() && woo_is_spare_parts_terms() ) {
    get_template_part( 'template-parts/modals/special-order' );
  }

  if ( is_singular( 'cpt-career' ) ) {
    get_template_part( 'template-parts/modals/submit-cv' );
  }

  echo '<script src="https://fast.wistia.com/assets/external/E-v1.js" async></script>';
}

add_action( 'elementor/theme/before_do_header', 'before_do_header', 0 );
function before_do_header() {
  echo '<div id="page">';
}

add_action( 'elementor/theme/after_do_footer', 'after_do_footer', 99 );
function after_do_footer() {
  echo '</div> <!-- #page -->';
}

add_filter( 'redirection_role', 'custom_redirection_role' );
function custom_redirection_role( $cap ) {
  return 'activate_plugins';
}

add_filter( 'wpcf7_dynamic_select_consult_types', 'cf7_dynamic_select_do_consult_types', 10, 2 );
function cf7_dynamic_select_do_consult_types( $choices, $args = array() ) {
  return array_flip( CONSULTATION_TYPES );
}

add_filter( 'wpcf7_mail_components', 'custom_adjust_wpcf7_mail_components', 10, 2 );
function custom_adjust_wpcf7_mail_components( $components, $contact_form ) {
  $submission = WPCF7_Submission::get_instance();

  if ( $submission ) {
    $posted_data = $submission->get_posted_data();

    if ( isset( $posted_data['your-pc'] ) ) {
      $post_code = $posted_data['your-pc'];
    } elseif ( isset( $posted_data['your-postcode'] ) ) {
      $post_code = $posted_data['your-postcode'];
    } elseif ( $posted_data['ZIP'] ) {
      $post_code = $posted_data['ZIP'];
    } elseif ( $posted_data['postcode'] ) {
      $post_code = $posted_data['postcode'];
    }

    if ( ! empty( $post_code ) ) {
      // Cc email base on post_code.
      $cc_emails = woo_get_cc_emails_by_postcode( sanitize_text_field( $post_code ) );

      if ( ! empty( $cc_emails ) ) {
        $cc_emails = implode( ', ', $cc_emails );

        $additional_headers = @$components['additional_headers'];

        // If have additional_headers
        if ( ! empty( $additional_headers ) ) {
          $additional_headers = explode( PHP_EOL, $additional_headers );
          $added_cc_header    = false;

          // Append cc if have cc header.
          foreach ( $additional_headers as $key => $additional_header ) {
            if ( 0 === stripos( $additional_header, 'CC' ) ) {
              $additional_headers[ $key ] .= ', ' . $cc_emails;
              $added_cc_header            = true;
              break;
            }
          }

          if ( ! $added_cc_header ) {
            array_unshift( $additional_headers, 'CC: ' . $cc_emails );
          }
          $components['additional_headers'] = implode( PHP_EOL, $additional_headers );
        } else {
          // dont have additional header
          $additional_headers               = 'CC: ' . $cc_emails;
          $components['additional_headers'] = $additional_headers;
        }
      }
    }
  }

  return $components;
}

add_action( 'admin_head', 'custom_admin_head' );
function custom_admin_head() {
  ?>
  <style>
      #adminmenu li.wp-menu-separator {
          border-bottom: 1px solid #5f5f5f;
      }

      .wpsl-opening-hours-tab {
          display: none;
      }
  </style>
  <?php
}

add_action( "wp_ajax_show_inspiration_modal_body", "ajax_show_inspiration_modal_body" );
add_action( "wp_ajax_nopriv_show_inspiration_modal_body", "ajax_show_inspiration_modal_body" );
function ajax_show_inspiration_modal_body() {
  global $post;

  $post_id = ! empty( $_GET['postId'] ) ? absint( $_GET['postId'] ) : 0;

  if ( ! ( $_post = get_post( $post_id ) ) ) {
    wp_send_json_error();
  }

  $post = $_post;
  setup_postdata( $post );

  ob_start();
  ?>
  <div class="row">
    <div class="col-lg-6 col-12 p-0">
      <?
      if ( ! empty( $wistia_videos = get_field( 'wistia_videos' ) ) ) {
        foreach ( $wistia_videos as $video ) {
          $esc_attr = esc_attr( $video['wistia_video_id'] );
          ?>

          <?/*
                <video
                  class="video-js video-js--manual"
                  controls
                  preload="auto"
                  data-setup='{"fluid": true, "responsive": true}'
                >
                  <source src="<?= $video['video']['url'] ?>" type="<?= esc_attr( $video['video']['mime_type'] ) ?>">
                  <p class="vjs-no-js">
                    To view this video please enable JavaScript, and consider upgrading to a
                    web browser that supports HTML5 video
                  </p>
                </video>
                */ ?>

          <script src="https://fast.wistia.com/embed/medias/<?= $esc_attr ?>.jsonp" async></script>
          <div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;">
            <div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_<?= $esc_attr ?> popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;position:relative;width:100%">&nbsp;</span></div>
          </div>
          <?php
        }
      }

      if ( has_post_thumbnail( $post ) ): $metadata = wp_get_attachment_metadata( get_post_thumbnail_id( $post ) ); ?>
        <div style="background-image: url(<?= get_the_post_thumbnail_url( $post, ( 'image/gif' == $metadata['sizes']['large']['mime-type'] ) ? 'full' : 'large' ) ?>)" class="modal__bg-image"></div>
      <? endif; ?>
    </div>

    <div class="col-lg-6 col-12 color-black p-0 bg-lightest-grey">
      <div class="p-3 py-lg-5 px-lg-5 bg-white">
        <div class="pt-lg-3 text-inner">
          <h4 class="text-center text-lg-left"><?= $post->post_title; ?></h4>
          <?= wpautop( $post->post_content );  // Do not use the_content()                      ?>
        </div>
      </div>

      <? if ( ! empty( $related_fabric = get_field( 'related_fabric' ) ) ) : ?>
        <?
        /** @var WP_Post $related_fabric */
        ?>
        <div class="p-3 px-lg-5 pb-lg-5 pt-lg-4">
          <img class="pattern-bg mb-2 mb-lg-4 mx-auto mx-lg-0 d-block" src="<?= get_the_post_thumbnail_url( $related_fabric, 'thumbnail' ); ?>" alt="<?= esc_attr( $related_fabric->post_title ) ?>">

          <h5 class="mb-3 text-center text-lg-left"><?= $related_fabric->post_title ?></h5>

          <div class="row be-inspired-stats">
            <?
            wc_setup_product_data( $related_fabric->ID );
            get_template_part( 'template-parts/sections/product-attributes', null, [ 'wrapper_classes' => '' ] );
            wp_reset_postdata();
            ?>
          </div>

          <div class="text-center text-lg-left">
            <a href="#" class="btn btn--order" onclick="Woo.toggleProductAjaxModal(<?= $related_fabric->ID ?>); return false;">Order sample</a>

            <? get_template_part( 'template-parts/explore-link' ) ?>
          </div>
        </div>
      <? endif; ?>
    </div>
  </div>
  <?php

  wp_reset_postdata();

  wp_send_json_success( [
    'html' => ob_get_clean(),
  ] );
}
