<?php

////////////////////////////////////////////////////////////////////////////////////////
// COMMON
// Customize default woocommerce hooks
add_action( 'wp', 'woo_customize_woocommerce', 99 );
function woo_customize_woocommerce() {
  global $product;

  if ( woo_is_fabric_sample_terms() ) {
    remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
  }

  if ( is_product() ) {
    add_action( 'woocommerce_single_product_summary', 'woocommerce_breadcrumb', 4 );
  }
}

add_action( 'init', 'woo_customize_woocommerce_init' );
function woo_customize_woocommerce_init() {
  remove_action( 'admin_notices', array( 'WDRPro\App\Controllers\Admin\UpdateHandler', 'errorNoticeInAdminPages' ) );
}

add_action( 'wp_enqueue_scripts', 'woo_enqueue_scripts', 99 );
function woo_enqueue_scripts() {
  if ( woo_has_fabric_modal() ) {
    WC_Frontend_Scripts::load_scripts();
    wp_enqueue_script( 'wc-single-product' );
    wp_enqueue_script( 'wc-add-to-cart' );
    wp_enqueue_script( 'wc-add-to-cart-variation' );

    if ( current_theme_supports( 'wc-product-gallery-lightbox' ) ) {
      wp_enqueue_script( 'photoswipe-ui-default' );
      wp_enqueue_style( 'photoswipe-default-skin' );
      add_action( 'wp_footer', 'woocommerce_photoswipe' );
    }
  }
}

add_action( 'elementor/theme/before_do_header', 'woo_before_do_header' );
function woo_before_do_header() {
  if ( woo_has_fabric_modal() ) {
    get_template_part( 'template-parts/modals/fabric-opacity' );
  }
}

add_action( 'woocommerce_before_quantity_input_field', 'woo_before_quantity_input_field' );
function woo_before_quantity_input_field() {
  if ( is_product() || is_product_taxonomy() ) {
    echo '<span class="font-weight-medium">Qty:</span>';
  }
}

add_action( 'after_setup_theme', 'woo_setup_theme' );
function woo_setup_theme() {
  add_theme_support( 'wc-product-gallery-slider' );
  add_theme_support( 'wc-product-gallery-lightbox' );
  add_theme_support( 'woocommerce' );
}

add_filter( 'woocommerce_breadcrumb_defaults', 'woo_change_breadcrumb_delimiter' );
function woo_change_breadcrumb_delimiter( $defaults ) {
  $defaults['delimiter'] = ' » ';

  return $defaults;
}

add_filter( 'woocommerce_loop_add_to_cart_args', 'woo_loop_add_to_cart_args' );
function woo_loop_add_to_cart_args( $args ) {
  $args['class'] .= ' btn--arrow ml-auto';
  $args['class'] = preg_replace( '/\bbutton\b/', '', $args['class'] );

  return $args;
}

add_filter( 'woocommerce_product_variation_title_include_attributes', '__return_false', 10, 2 );
//add_filter( 'woocommerce_product_variation_title_include_attributes', 'woo_product_variation_title_include_attributes', 10, 2 );
function woo_product_variation_title_include_attributes( $should_include_attributes, $_product ) {
  if ( woo_product_is_fabric_samples( $_product ) ) {
    return false;
  }

  return $should_include_attributes;
}

// add_filter( 'woocommerce_attribute_label', 'woo_attribute_label', 10, 3 );
function woo_attribute_label( $label, $name, $product ) {
  $parts = explode( '|', $label );

  return end( $parts );
}

add_filter( 'woocommerce_product_add_to_cart_text', 'woo_product_add_to_cart_text' );
function woo_product_add_to_cart_text( $text ) {
  global $woocommerce_loop;

  if ( is_product_taxonomy() || $woocommerce_loop['name'] == 'related' ) {
    return str_replace( [ 'Read more', 'Select options', 'Add to cart' ], '', $text );
  }

  return $text;
}

////////////////////////////////////////////////////////////////////////////////////////
// CUSTOMIZE EMAILS

////////////////////////////////////////////////////////////////////////////////////////
// CUSTOMIZE ADMIN
add_filter( 'yith_plugin_fw_show_eciton_esnecil_etavitca', '__return_false' );
add_filter( 'woocommerce_helper_suppress_admin_notices', '__return_true' );

add_filter( 'woocommerce_attribute_show_in_nav_menus', 'wc_reg_for_menus', 1, 2 );
function wc_reg_for_menus( $register, $name = '' ) {
  if ( in_array( $name, [
    'pa_product-type',
    'pa_space',
    'pa_room',
    'pa_purpose',
  ] ) ) {
    return true;
  }

  return $register;
}

add_filter( "woocommerce_taxonomy_args_pa_spare-parts", 'woo_taxonomy_args_pa_spare_parts' );
function woo_taxonomy_args_pa_spare_parts( $taxonomy_data ) {
  $taxonomy_data['hierarchical'] = true;

  return $taxonomy_data;
}

add_action( 'woocommerce_product_after_variable_attributes', 'woo_variation_settings_fields', 10, 3 );
function woo_variation_settings_fields( $loop, $variation_data, $variation ) {
  woocommerce_wp_select( array(
      'id'                => KEY_VAR_COLOR_SCHEME . '_' . $variation->ID,
      'name'              => KEY_VAR_COLOR_SCHEME . '[' . $variation->ID . '][]',
      'label'             => __( 'Colour Scheme', 'woocommerce' ),
      'wrapper_class'     => 'form-row form-row-full',
      'value'             => call_user_func( function () use ( $variation ) {
        $schemes = get_post_meta( $variation->ID, KEY_VAR_COLOR_SCHEME, true );

        if ( ! empty( $schemes ) && is_string( $schemes ) ) {
          return explode( ',', $schemes );
        }

        return [];
      } ),
      'desc_tip'          => true,
      'custom_attributes' => [ 'multiple' => 'multiple' ],
      'description'       => 'Control the color scheme for variation on filter, so filter will use the variation image instead of parent\'s image. We shouldn\'t add this attribute to parent product by the way.',
      'options'           => call_user_func( function () {
        $data = [ '' => 'Please select' ];

        $terms = get_terms( [
          'taxonomy'   => TERM_COLOUR_SCHEME,
          'hide_empty' => false,
        ] );

        if ( ! is_wp_error( $terms ) && ! empty( $terms ) ) {
          foreach ( $terms as $term ) {
            $data[ $term->slug ] = $term->name;
          }
        }

        return $data;
      } ),
    )
  );
}

add_action( 'woocommerce_save_product_variation', 'woo_save_variation_settings_fields', 10, 2 );
function woo_save_variation_settings_fields( $variation_id, $loop ) {
  $select = $_POST[ KEY_VAR_COLOR_SCHEME ][ $variation_id ];
  if ( ! empty( $select ) ) {
    update_post_meta( $variation_id, KEY_VAR_COLOR_SCHEME, implode( ',', $select ) );
  }
}

////////////////////////////////////////////////////////////////////////////////////////
// CUSTOMIZE ARCHIVE
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );

add_action( 'woocommerce_after_main_content', 'woo_after_main_content' );
function woo_after_main_content() {
  if ( is_product_taxonomy() ) {
    if ( woo_has_products_filter_topbar() ) {
      $queried_object = get_queried_object();

      $taxonomy = $queried_object->taxonomy;
      $term_id  = $queried_object->term_id;

      $next_term_id = get_adjacent_taxonomy_id( $taxonomy, $term_id );
      $prev_term_id = get_adjacent_taxonomy_id( $taxonomy, $term_id, true );

      if ( $next_term_id || $prev_term_id ) : ?>
        <div class="woocommerce-archive-nav">
          <div class="row no-gutters">
            <? if ( $prev_term_id ) : $term = get_term( $prev_term_id ) ?>
              <div class="col text-center">
                <a href="<?= get_term_link( $term ) ?>" class="d-block px-3 py-4">
                  <h5 class="mb-0"><img src="<? the_assets_path( 'images/arrow-left-black.svg' ); ?>" alt="Arrow" class="img-fluid" width="30"> <span class="text-right"><span class="inner-text">Previous to:</span><?= $term->name ?></span></h5>
                </a>
              </div>
            <? endif; ?>

            <? if ( $next_term_id ) : $term = get_term( $next_term_id ) ?>
              <div class="col text-center">
                <a href="<?= get_term_link( $term ) ?>" class="d-block px-3 py-4">
                  <h5 class="mb-0"><span class="text-left"><span class="inner-text">Next to:</span> <?= $term->name ?></span> <img src="<? the_assets_path( 'images/arrow-right-black.svg' ); ?>" alt="Arrow" class="img-fluid" width="30"></h5>
                </a>
              </div>
            <?php endif; ?>
          </div>
        </div>
      <?php endif;
    }
  }

  // "We are here to help" section
  get_template_part( 'template-parts/sections/here-to-help' );
}

add_action( 'woocommerce_shop_loop_item_title', 'woo_shop_loop_item_title', 9 );
function woo_shop_loop_item_title() {
  get_template_part( 'template-parts/sections/product-color-palette' );
}

add_action( 'woocommerce_after_shop_loop_item', 'woo_after_shop_loop_item', 6 );
function woo_after_shop_loop_item() {
  get_template_part( 'template-parts/sections/product-short-description' );
  get_template_part( 'template-parts/sections/product-attributes' );
}

add_filter( 'woocommerce_product_get_image', 'woo_product_get_image', 10, 5 );
function woo_product_get_image( $image, $_product, $size, $attr, $placeholder ) {
  // Show variation image instead of parent product image if colour scheme is in the filter query

  /** @var WC_Product_Variable $_product */
  if (
    ! empty( $_GET['filter_colour-scheme'] ) &&
    is_product_taxonomy() &&
    $_product->is_type( 'variable' ) &&
    woo_product_is_fabric_samples( $_product )
  ) {
    $colour_schemes = explode( ',', sanitize_text_field( $_GET['filter_colour-scheme'] ) );

    if ( ! empty( $available_variations = $_product->get_available_variations( 'objects' ) ) ) {
      /** @var WC_Product_Variation $available_variation */

      foreach ( $available_variations as $available_variation ) {
        $variation_schemes = $available_variation->get_meta( KEY_VAR_COLOR_SCHEME );

        if ( ! empty( $variation_schemes ) ) {
          $variation_schemes = explode( ',', $variation_schemes );

          if ( ! empty( array_intersect( $colour_schemes, $variation_schemes ) ) ) {
            remove_filter( 'woocommerce_product_get_image', 'woo_product_get_image', 10 );
            $variation_image = $available_variation->get_image( 'woocommerce_thumbnail' );
            add_filter( 'woocommerce_product_get_image', 'woo_product_get_image', 10, 5 );

            return $variation_image;
          }
        }
      }
    }

//    $found_variation_id = ( new \WC_Product_Data_Store_CPT() )->find_matching_product_variation(
//      new \WC_Product( $_product->get_id() ),
//      [
//        TERM_COLOUR_SCHEME => $colour_scheme,
//      ]
//    );
//
//    if ( ! empty( $found_variation_id ) ) {
//      $found_variation = wc_get_product( $found_variation_id );
//
//      /** @var WC_Product_Variation $found_variation */
//
//      return $found_variation->get_image( 'woocommerce_thumbnail' );
//    }
  }

  return $image;
}

add_filter( 'woocommerce_product_loop_title_classes', 'woo_product_loop_title_classes' );
function woo_product_loop_title_classes( $classes ) {
  return esc_attr( "$classes " . CLASSES_PRODUCT_LOOP_SPACING );
}

add_filter( 'yith_wcan_dropdown_label', 'woo_yith_wcan_dropdown_default_label', 10, 3 );
function woo_yith_wcan_dropdown_default_label( $dropdown_label, $navigation_widget, $instance ) {
  if ( woo_has_products_filter_topbar() ) {
    $tax_slug = "pa_{$instance['attribute']}";

    $taxonomy = get_taxonomy( $tax_slug );

    $parts = explode( '|', $taxonomy->labels->singular_name );

    return end( $parts );
  }

  return $dropdown_label;
}

add_filter( 'yith_wcan_get_woocommerce_layered_nav_link', 'woo_yith_wcan_get_woocommerce_layered_nav_link' );
function woo_yith_wcan_get_woocommerce_layered_nav_link( $return ) {
  if ( woo_has_products_filter_topbar() || woo_has_products_filter_sidebar() ) {
    $queried_object = get_queried_object();

    return get_term_link( $queried_object->term_id );
  }

  return $return;
}

add_action( 'woocommerce_before_shop_loop', 'woo_before_shop_loop' );
function woo_before_shop_loop() {
  get_template_part( 'template-parts/sections/archive-top' );
}

add_action( 'widgets_init', 'woo_widgets_init' );
function woo_widgets_init() {
  register_sidebar( array(
    'name'          => 'Filter Top Bar',
    'id'            => 'by_filter',
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h4 class="title">',
    'after_title'   => '</h4>',
  ) );

  register_sidebar( array(
    'name'          => 'Filter Fabric Samples',
    'id'            => 'filter_samples',
    'before_widget' => '<div id="%1$s" class="%2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h5 class="title">',
    'after_title'   => '</h5>',
  ) );

  register_sidebar( array(
    'name'          => 'Filter Spare Parts',
    'id'            => 'filter_spare_parts',
    'before_widget' => '<div id="%1$s" class="%2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h5 class="title">',
    'after_title'   => '</h5>',
  ) );
}

add_filter( 'widget_title', 'woo_custom_widget_title' );
function woo_custom_widget_title( $title ) {
  if ( woo_has_products_filter_sidebar() ) {
    $title = sprintf( '<span class="title-inner">%s</span><span class="title-toggle"></span>', $title );
  }

  return $title;
}

////////////////////////////////////////////////////////////////////////////////////////
// CUSTOMIZE CHECKOUT
remove_action( 'woocommerce_checkout_order_review', 'woocommerce_checkout_payment', 20 );
add_action( 'woocommerce_checkout_after_customer_details', 'woocommerce_checkout_payment', 99 );

add_filter( 'woocommerce_checkout_fields', 'custom_wc_checkout_fields_no_label' );
function custom_wc_checkout_fields_no_label( $fields ) {
  foreach ( $fields as $category => $value ) {
    foreach ( $fields[ $category ] as $field => $property ) {
      unset( $fields[ $category ][ $field ]['label'] );
    }
  }

  return $fields;
}

add_filter( 'default_checkout_billing_country', 'woo_change_default_checkout_country', 10, 1 );
function woo_change_default_checkout_country( $country ) {
  // If the user already exists, don't override country
  if ( WC()->customer->get_is_paying_customer() ) {
    return $country;
  }

  return 'AU';
}

add_action( 'woocommerce_cart_calculate_fees', 'woo_custom_free_samples' );
function woo_custom_free_samples() {
  global $woocommerce;

  if ( $sample_quantity = woo_get_samples_quantity_in_cart() ) {
    $woocommerce->cart->add_fee( 'Free Samples', - min( $sample_quantity, 5 ), true, 'zero-rate' );
  }
}

add_action( 'woocommerce_cart_totals_get_fees_from_cart_taxes', 'woo_exclude_cart_fees_taxes', 10, 3 );
function woo_exclude_cart_fees_taxes( $taxes, $fee, $cart ) {
  return [];
}

////////////////////////////////////////////////////////////////////////////////////////
// CUSTOMIZE CART

remove_action( 'woocommerce_widget_shopping_cart_buttons', 'woocommerce_widget_shopping_cart_button_view_cart', 10 );
remove_action( 'woocommerce_widget_shopping_cart_buttons', 'woocommerce_widget_shopping_cart_proceed_to_checkout', 20 );
remove_action( 'woocommerce_widget_shopping_cart_total', 'woocommerce_widget_shopping_cart_subtotal', 10 );
remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display', 10 );

// add_filter( 'woocommerce_shipping_calculator_enable_country', '__return_false');
// add_filter( 'woocommerce_shipping_calculator_enable_city', '__return_false');
// add_filter( 'woocommerce_shipping_calculator_enable_postcode', '__return_false');

add_action( 'woocommerce_widget_shopping_cart_before_buttons', 'woo_widget_shopping_cart_subtotal', 10 );
function woo_widget_shopping_cart_subtotal() {
  wc_get_template( 'cart/cart-totals.php' );
}

// add_action( 'woocommerce_widget_shopping_cart_buttons', 'woo_widget_shopping_cart_button_view_cart', 10 );
function woo_widget_shopping_cart_button_view_cart() {
  echo '<a href="' . esc_url( wc_get_cart_url() ) . '" class="btn btn--view-cart">' . esc_html__( 'Edit cart', 'woocommerce' ) . '</a>';
}

// add_action( 'woocommerce_widget_shopping_cart_buttons', 'woo_widget_shopping_cart_proceed_to_checkout', 20 );
function woo_widget_shopping_cart_proceed_to_checkout() {
  echo '<a href="' . esc_url( wc_get_checkout_url() ) . '" class="button checkout wc-forward">' . esc_html__( 'Checkout now', 'woocommerce' ) . '</a>';
}

add_filter( 'woocommerce_add_to_cart_fragments', 'woo_filter_woocommerce_update_order_review_fragments', 10, 1 );
function woo_filter_woocommerce_update_order_review_fragments( $fragments ) {
  $fragments['.woo-cart__count'] = '<span class="woo-cart__count">' . ( ( $contents_count = WC()->cart->get_cart_contents_count() ) ? $contents_count : '' ) . '</span>';

  return $fragments;
}

add_filter( 'woocommerce_cart_shipping_method_full_label', 'woo_cart_shipping_method_full_label' );
function woo_cart_shipping_method_full_label( $label ) {
  $label = str_replace('Standard Shipping:', '', $label);

  return $label;
}

add_filter( 'woocommerce_get_cart_contents', 'woo_get_cart_contents', 10 );
function woo_get_cart_contents( $cart_contents ) {
  $new_group_items = WOO_GROUP_ITEMS;

  foreach ( $cart_contents as $cart_item_key => $cart_item ) {
    if ( isset( $cart_item['item_group'], $new_group_items[ sanitize_key( $cart_item['item_group'] ) ] ) ) {
      $new_group_items[ sanitize_key( $cart_item['item_group'] ) ][ $cart_item_key ] = $cart_item;
    } else {
      $new_group_items['item_group_others'][ $cart_item_key ] = $cart_item;
    }
  }

  return call_user_func_array( 'array_merge', $new_group_items );
}

add_filter( 'woocommerce_add_cart_item_data', 'woo_add_cart_item_data', 10, 3 );
function woo_add_cart_item_data( $cart_item_data, $product_id, $variation_id ) {
  if ( woo_product_is_fabric_samples( $product_id ) ) {
    $cart_item_data['item_group'] = 'item_group_fabric';
  } elseif ( woo_product_is_wynstan_health( $product_id ) ) {
    $cart_item_data['item_group'] = 'item_group_wynstan_health';
  } elseif ( woo_product_is_spare_parts( $product_id ) ) {
    $cart_item_data['item_group'] = 'item_group_spare_parts';
  }

  return $cart_item_data;
}

add_filter( 'woocommerce_update_cart_validation', 'woo_limit_fabrics_on_qty_update', 10, 4 );
function woo_limit_fabrics_on_qty_update( $valid, $cart_item_key, $values, $quantity ) {
  $cart_contents = WC()->cart->get_cart_contents();

  try {
    return ( $valid && woo_validate_fabric_in_cart(
        (isset($cart_contents[ $cart_item_key ]['variation_id']) ? $cart_contents[ $cart_item_key ]['variation_id'] : $cart_contents[ $cart_item_key ]['product_id']),
        $quantity - $cart_contents[ $cart_item_key ]['quantity'],
        false ) );
  } catch ( Exception $e ) {
  }

  return $valid;
}

add_filter( 'woocommerce_add_cart_item_data', 'woo_limit_fabrics', 10, 4 );
function woo_limit_fabrics( $cart_item_data, $product_id, $variation_id, $quantity ) {
  /* Don not try catch below */
  woo_validate_fabric_in_cart( $variation_id, $quantity );

  return $cart_item_data;
}

////////////////////////////////////////////////////////////////////////////////////////
// CUSTOMIZE PRODUCT SINGLE

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );

add_action( 'woocommerce_single_product_summary', 'woo_single_product_summary' );
function woo_single_product_summary() {
  get_template_part( 'template-parts/sections/product-info' );
}

add_action( 'woocommerce_after_single_product', 'woo_after_single_product' );
function woo_after_single_product() {
  ?>
  <script>
    jQuery(document).ready(function($) {
      $('.read-more').readmore({
        speed: 500,
        moreLink: '<a href="#" class="pink">Read More <img src="<? the_assets_path( 'images/plus.svg' ); ?>" alt="Plus" class="img-fluid" width="10" style="vertical-align:-1px;"></a>',
        lessLink: '<a href="#" class="pink">Less</a>'
      });
    });
  </script>
  <?php
}

add_action( 'woocommerce_before_single_product_summary', 'woo_show_product_gallery', 10 );
function woo_show_product_gallery() {
  global $product;

  if ( ! function_exists( 'wc_get_gallery_image_html' ) ) {
    return;
  }

  if ( ! woo_product_is_normal_product( $product ) ) {
    return;
  }

  remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20 );

  get_template_part( 'template-parts/sections/product-custom-gallery' );
}

add_filter( 'woocommerce_dropdown_variation_attribute_options_html', 'woo_dropdown_variation_attribute_options_html', 10, 2 );
function woo_dropdown_variation_attribute_options_html( $html, $args ) {
  // Support ajax product modal only
  if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
    // Get selected value.
    if ( false === $args['selected'] && $args['attribute'] && $args['product'] instanceof WC_Product ) {
      $selected_key = 'attribute_' . sanitize_title( $args['attribute'] );
      // phpcs:disable WordPress.Security.NonceVerification.Recommended
      $args['selected'] = isset( $_REQUEST[ $selected_key ] ) ? wc_clean( wp_unslash( $_REQUEST[ $selected_key ] ) ) : $args['product']->get_variation_default_attribute( $args['attribute'] );
      // phpcs:enable WordPress.Security.NonceVerification.Recommended
    }

    $options               = $args['options'];
    $product               = $args['product'];
    $attribute             = $args['attribute'];
    $name                  = $args['name'] ? $args['name'] : 'attribute_' . sanitize_title( $attribute );
    $id                    = $args['id'] ? $args['id'] : sanitize_title( $attribute );
    $class                 = $args['class'];
    $show_option_none      = (bool) $args['show_option_none'];
    $show_option_none_text = $args['show_option_none'] ? $args['show_option_none'] : __( 'Choose an option', 'woocommerce' ); // We'll do our best to hide the placeholder, but we'll need to show something when resetting options.

    if ( empty( $options ) && ! empty( $product ) && ! empty( $attribute ) ) {
      $attributes = $product->get_variation_attributes();
      $options    = $attributes[ $attribute ];
    }

    if ( ! empty( $options ) ) {
      $html .= '<div class="woo-custom-checkboxes ' . ( TERM_COLOUR == $args['attribute'] ? 'colors-palette d-flex' : '' ) . '">';
      if ( $product && taxonomy_exists( $attribute ) ) {
        // Get terms if this is a taxonomy - ordered. We need the names too.
        $terms = wc_get_product_terms(
          $product->get_id(),
          $attribute,
          array(
            'fields' => 'all',
          )
        );

        /** @var WC_Product_Variable $product */
        $variations = $product->get_available_variations( 'objects' );

        foreach ( $terms as $term ) {
          if ( in_array( $term->slug, $options, true ) ) {
            $html .= '<label class="radio-holder">';
            $html .= '<input type="radio" class="woo-custom-checkboxes woo-custom-checkboxes--variations" data-target="' . esc_attr( $name ) . '" name="target-' . esc_attr( $name ) . '" value="' . esc_attr( $term->slug ) . '" ' . checked( sanitize_title( $args['selected'] ), $term->slug, false ) . '/>';
            $html .= '<span class="radio-label">';
            switch ( $args['attribute'] ) {
              case TERM_COLOUR:
                $found_colours = array_filter( $variations, function ( $variation ) use ( $term ) {
                  $attributes = $variation->get_attributes();

                  return in_array( TERM_COLOUR, array_keys( $attributes ) ) && ( $term->slug == $attributes['pa_colour'] );
                } );

                /** @var WC_Product_Variable $found_colour */
                $found_colour = array_pop( $found_colours );

                $html .= '<div class="colors-palette__item"><div class="colors-palette__img">' . $found_colour->get_image( 'thumbnail' ) . '</div><div class="colors-palette__title">' . esc_html( apply_filters( 'woocommerce_variation_option_name', $term->name, $term, $attribute, $product ) ) . '</div></div>';
                break;
              default:
                $html .= '<span class="badge badge-primary">' . esc_html( apply_filters( 'woocommerce_variation_option_name', $term->name, $term, $attribute, $product ) ) . '</span>';
            }
            $html .= '</span>';
            $html .= '</label>';
          }
        }

        if ( TERM_OPACITY == $args['attribute'] ) {
          ob_start()
          ?>
          <a href="#" data-toggle="modal" data-target="#modal-fabric-opacity"><img src="<? the_assets_path( 'images/question.svg' ); ?>" alt="Question" class="img-fluid" width="17"></a>
          <?php
          $html .= ob_get_clean();
        }
      }
      $html .= '</div>';
    }
  }

  return $html;
}

add_filter( 'woocommerce_product_single_add_to_cart_text', 'woo_product_single_add_to_cart_text', 10, 2 );
function woo_product_single_add_to_cart_text( $text, $_product ) {
  if ( woo_product_is_fabric_samples( $_product ) ) {
    return 'Add fabric sample to cart';
  }

  return $text;
}

add_filter( 'woocommerce_reset_variations_link', 'woo_reset_variations_link' );
function woo_reset_variations_link( $link ) {
  return '<a class="reset_variations" href="#">' . esc_html__( 'Clear selections', 'woocommerce' ) . '</a>';
}

add_filter( 'woocommerce_post_class', 'woo_post_class', 10, 2 );
function woo_post_class( $classes, $_product ) {
  $classes[] = 'clearfix';

  /** @var WC_Product $_product */
  if ( woo_product_is_fabric_samples( $_product ) ) {
    $classes[] = 'product-type--fabric';
  }

  return $classes;
}

add_filter( 'woocommerce_output_related_products_args', 'woo_related_products_args', 20 );
function woo_related_products_args( $args ) {
  $args['posts_per_page'] = 3;
  $args['columns']        = 3;

  return $args;
}

add_filter( 'woocommerce_quantity_input_max', 'woo_quantity_input_max', 10, 2 );
function woo_quantity_input_max( $max, $product ) {
  if ( $max < 0 && woo_product_is_fabric_samples( $product ) ) {
    return MAX_SAMPLES_PER_PRODUCT;
  }

  return $max;
}

add_filter( 'woocommerce_quantity_input_args', 'woo_quantity_input_args', 10, 2 );
function woo_quantity_input_args( $args, $product ) {
  if ( $args['max_value'] < 0 && woo_product_is_fabric_samples( $product ) ) {
    $args['max_value'] = MAX_SAMPLES_PER_PRODUCT;
  }

  return $args;
}

add_filter( 'woocommerce_available_variation', 'woo_available_variation', 10, 2 );
function woo_available_variation( $args, $product ) {
  if ( ! $args['max_qty'] && woo_product_is_fabric_samples( $product ) ) {
    $args['max_qty'] = MAX_SAMPLES_PER_PRODUCT;
  }

  return $args;
}

////////////////////////////////////////////////////////////////////////////////////////
// CUSTOMIZE EMAIL

add_filter( 'wpo_wcpdf_settings_fields_documents_packing_slip', 'woo_custom_wcpdf_settings_fields_documents_packing_slip', 10, 4 );
function woo_custom_wcpdf_settings_fields_documents_packing_slip( $settings_fields, $page, $option_group, $option_name ) {
  $settings_fields[] = array(
    'type'     => 'setting',
    'id'       => 'attach_to_email_ids',
    'title'    => __( 'Attach to:', 'woocommerce-pdf-invoices-packing-slips' ),
    'callback' => 'multiple_checkboxes',
    'section'  => 'packing_slip',
    'args'     => array(
      'option_name' => $option_name,
      'id'          => 'attach_to_email_ids',
      'fields'      => [ "new_order" => 'New order (Admin email)' ],
      'description' => '',
    ),
  );

  return $settings_fields;
}

////////////////////////////////////////////////////////////////////////////////////////
// AJAX
add_action( 'wp_ajax_nopriv_get_ajax_products', 'woo_get_ajax_products' );
add_action( 'wp_ajax_get_ajax_products', 'woo_get_ajax_products' );
function woo_get_ajax_products() {
  global $post;
  global $product;

  $show_more_fabric_link = is_product() && woo_product_is_normal_product( $product );

  $product_id = ( isset( $_POST['product_id'] ) ) ? absint( $_POST['product_id'] ) : '';

  $post = get_post( $product_id );
  setup_postdata( $post );
  wc_setup_product_data( $product_id );

  ob_start();

  get_template_part( 'template-parts/sections/product-order-fabric', null, [ 'show_more_fabric_link' => $show_more_fabric_link ] );

  $html = ob_get_clean();

  wp_reset_postdata();

  wp_send_json_success( [
    'html' => $html,
  ] );
}
