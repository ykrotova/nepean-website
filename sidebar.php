<?php
/**
 * The template for displaying sidebar.
 *
 * @package HelloElementor
 */

if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly.
}

/**
 * This file is here to avoid the Deprecated Message for sidebar by wp-includes/theme-compat/sidebar.php.
 */

if ( woo_has_products_filter_sidebar() ) : ?>
  <div class="mb-4">
    <? if ( woo_is_fabric_sample_terms() ) : ?>
      <? ! is_active_sidebar( 'filter_samples' ) or dynamic_sidebar( 'filter_samples' ); ?>
    <? elseif ( woo_is_spare_parts_terms() ) : ?>
      <? ! is_active_sidebar( 'filter_spare_parts' ) or dynamic_sidebar( 'filter_spare_parts' ); ?>

      <h6 class="border-top pt-3" style="font-size: 16px;">Can’t find what you are looking for? <a href="#" data-toggle="modal" data-target="#modal-special-order"><u>We can help</u></a></h6>
    <? endif; ?>
  </div>
<? endif;
