<?php

$text  = $args['text'] ?? '';
$link  = $args['link'] ?? false;
$order = $args['order'] ?? false;
$class = $args['class'] ?? '';
?>

<?php if ( $link ): ?>
  <a href="<?= $link; ?>" class="elementor-button-link elementor-button elementor-size-sm <?= ( $class ) ? $class : ''; ?><?= ( $order ) ? ' btn--order' : ''; ?>" role="button">
        <span class="elementor-button-content-wrapper">
            <?php if ( $order ): ?>
              <div class="overflow-hidden position-relative">
                    <img class="img-fluid mr-2" src="<?= get_assets_path( 'images/icon-order-cart.svg' ); ?>" alt="">

                    <div class="circle-wrap">
                        <i aria-hidden="true" class="fas fa-circle-notch fa-spin color-primary"></i>
                    </div>
                </div>
            <?php endif; ?>

            <span class="elementor-button-text"><?= ( $text ) ? $text : ''; ?></span>
        </span>
  </a>
<?php endif; ?>
