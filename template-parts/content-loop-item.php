<?php

$image = $args['image'] ?? '';
$link  = $args['link'] ?? '';
$cat   = $args['cat'] ?? '';
$title = $args['title'] ?? '';
$text  = $args['text'] ?? '';
?>

<?php if ( $image && $link && $title ): ?>
  <div class="loop-item">
    <div class="position-relative overflow-hidden">
      <a href="<?= $link; ?>" class="loop-item__image" style="background-image: url(<?= $image; ?>)"></a>

      <?php if ( $cat ): ?>
        <a class="loop-item__cat" href="#"><?= $cat; ?></a>
      <?php endif; ?>
    </div>

    <div class="loop-item__text balance-elements">
      <a href="<?= $link; ?>">
        <h5 class="loop-item__title"><?= $title; ?></h5>

        <?php if ( $text ): ?>
          <p><?= $text; ?></p>
        <?php endif; ?>

        <span class="btn--arrow"></span>
      </a>
    </div>
  </div>
<?php endif;
