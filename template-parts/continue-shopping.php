<p class="text-center mt-4 link--continue-shopping">
  <? if ( isset( $args['toggle_cart'] ) && $args['toggle_cart'] ) : ?>
    <a href="#" onclick="Woo.toggleCart(); return false;" class="btn--arrow-link btn--arrow-link--reverse">Continue shopping</a>
  <? else: ?>
    <a href="<? echo esc_url( apply_filters( 'woocommerce_return_to_shop_redirect', wc_get_page_permalink( 'shop' ) ) ); ?>" class="btn--arrow-link btn--arrow-link--reverse">Continue shopping</a>
  <? endif; ?>
</p>
