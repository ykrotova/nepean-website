<?php

global $wpsl_settings, $wpsl;

$output         = $this->get_custom_css();
$autoload_class = ( ! $wpsl_settings['autoload'] ) ? 'class="wpsl-not-loaded"' : '';

$output .= '<div id="wpsl-wrap">' . "\r\n";
$output .= "\t" . '<div class="wpsl-search wpsl-clearfix ' . $this->get_css_classes() . '">' . "\r\n";
$output .= "\t" . '<h1 class="d-lg-block d-none mt-5 mb-3">Find a showroom</h1>';
$output .= "\t\t" . '<div id="wpsl-search-wrap">' . "\r\n";
$output .= "\t\t\t" . '<form autocomplete="off" class="d-flex">' . "\r\n";
$output .= "\t\t\t" . '<div class="wpsl-input">' . "\r\n";
$output .= "\t\t\t\t" . '<div><label for="wpsl-search-input" class="sr-only">' . esc_html( $wpsl->i18n->get_translation( 'search_label', __( 'Your location', 'wpsl' ) ) ) . '</label></div>' . "\r\n";
$output .= "\t\t\t\t" . '<div class="input-wrapper position-relative w-100">';
$output .= "\t\t\t\t\t" . '<input id="wpsl-search-input" type="text" value="' . apply_filters( 'wpsl_search_input', '' ) . '" name="wpsl-search-input" placeholder="Enter your location" aria-required="true" />' . "\r\n";
$output .= "\t\t\t\t" . '</div>' . "\r\n";
$output .= "\t\t\t" . '</div>' . "\r\n";

if ( $wpsl_settings['radius_dropdown'] || $wpsl_settings['results_dropdown'] ) {
  $output .= "\t\t\t" . '<div class="wpsl-select-wrap">' . "\r\n";

  if ( $wpsl_settings['radius_dropdown'] ) {
    $output .= "\t\t\t\t" . '<div id="wpsl-radius">' . "\r\n";
    $output .= "\t\t\t\t\t" . '<label for="wpsl-radius-dropdown">' . esc_html( $wpsl->i18n->get_translation( 'radius_label', __( 'Search radius', 'wpsl' ) ) ) . '</label>' . "\r\n";
    $output .= "\t\t\t\t\t" . '<select id="wpsl-radius-dropdown" class="wpsl-dropdown" name="wpsl-radius">' . "\r\n";
    $output .= "\t\t\t\t\t\t" . $this->get_dropdown_list( 'search_radius' ) . "\r\n";
    $output .= "\t\t\t\t\t" . '</select>' . "\r\n";
    $output .= "\t\t\t\t" . '</div>' . "\r\n";
  }

  if ( $wpsl_settings['results_dropdown'] ) {
    $output .= "\t\t\t\t" . '<div id="wpsl-results">' . "\r\n";
    $output .= "\t\t\t\t\t" . '<label for="wpsl-results-dropdown">' . esc_html( $wpsl->i18n->get_translation( 'results_label', __( 'Results', 'wpsl' ) ) ) . '</label>' . "\r\n";
    $output .= "\t\t\t\t\t" . '<select id="wpsl-results-dropdown" class="wpsl-dropdown" name="wpsl-results">' . "\r\n";
    $output .= "\t\t\t\t\t\t" . $this->get_dropdown_list( 'max_results' ) . "\r\n";
    $output .= "\t\t\t\t\t" . '</select>' . "\r\n";
    $output .= "\t\t\t\t" . '</div>' . "\r\n";
  }

  $output .= "\t\t\t" . '</div>' . "\r\n";
}

if ( $this->use_category_filter() ) {
  $output .= $this->create_category_filter();
}

$output .= "\t\t\t\t" . '<div class="wpsl-search-btn-wrap"><button id="wpsl-search-btn" type="submit" title="' . esc_attr( $wpsl->i18n->get_translation( 'search_btn_label', __( 'Search', 'wpsl' ) ) ) . '"><img src="' . get_assets_path( 'images/point.svg' ) . '" class="img-fluid" width="20"></button></div>' . "\r\n";

$output .= "\t\t" . '</form>' . "\r\n";
$output .= '<p class="mt-2" style="font-size:14px;"><img class="img-fluid" width="24" src="' . get_assets_path( 'images/aim.svg' ) . '" alt="Location"> Your current location</p>';
$output .= "\t\t" . '</div>' . "\r\n";
$output .= "\t" . '</div>' . "\r\n";

$output .= "\t" . '<div id="wpsl-gmap" class="wpsl-gmap-canvas"></div>' . "\r\n";

$output .= "\t" . '<div id="wpsl-result-list">' . "\r\n";
$output .= "\t\t" . '<div id="wpsl-stores" ' . $autoload_class . '>' . "\r\n";
$output .= "\t\t\t" . '<ul></ul>' . "\r\n";
$output .= "\t\t" . '</div>' . "\r\n";
$output .= "\t\t" . '<div id="wpsl-direction-details">' . "\r\n";
$output .= "\t\t\t" . '<ul></ul>' . "\r\n";
$output .= "\t\t" . '</div>' . "\r\n";
$output .= "\t" . '</div>' . "\r\n";

if ( $wpsl_settings['show_credits'] ) {
  $output .= "\t" . '<div class="wpsl-provided-by">' . sprintf( __( "Search provided by %sWP Store Locator%s", "wpsl" ), "<a target='_blank' href='https://wpstorelocator.co'>", "</a>" ) . '</div>' . "\r\n";
}

$output .= '</div>' . "\r\n";

return $output;
