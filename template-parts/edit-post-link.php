<?

global $post;

if ( current_user_can( 'edit_posts' ) ) : ?>
  <a class="edit-post-link" href="<?= get_edit_post_link() ?>" target="_blank" style="<?= ! empty( $args['style'] ) ? esc_attr( $args['style'] ) : '' ?>">Edit <?= str_replace( [ 'cpt-' ], [ '' ], $post->post_type ) ?></a>
<? endif;
