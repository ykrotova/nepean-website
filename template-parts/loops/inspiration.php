<?php

global $post;
?>

<div class="grid-item">
  <a href="#" class="grid-item__inner-content" data-toggle="modal" data-target="#modal-be-inspired-<?= $post->ID ?>" title="<?= esc_attr( $post->post_title ) ?>">
    <? if ( ! empty( $wistia_videos = get_field( 'wistia_videos' ) ) ) : ?>
      <?
      $video = array_pop( $wistia_videos );

      $esc_attr = esc_attr( $video['wistia_video_id'] );
      ?>
      <script src="https://fast.wistia.com/embed/medias/<?= $esc_attr ?>.jsonp" async></script>
      <div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;">
        <div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_<?= $esc_attr ?> popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;position:relative;width:100%">&nbsp;</span></div>
      </div>

    <img src="<? the_assets_path( 'images/play.svg' ); ?>" alt="Play" class="img-fluid grid-item__play" width="60">
    <? else: ?>
    <img src="<?= get_the_post_thumbnail_url( $post, 'medium_large' ) ?>" alt="<?= esc_attr( get_the_title() ) ?>">
    <? endif; ?>

    <? if ( ! empty( $related_fabric = get_field( 'related_fabric' ) ) ) : ?>
      <img class="grid-item__pattern-image" src="<?= get_the_post_thumbnail_url( $related_fabric, 'thumbnail' ); ?>" alt="<?= esc_attr( $related_fabric->post_title ) ?>">
    <? endif; ?>
  </a>

  <? get_template_part( 'template-parts/edit-post-link' ) ?>

  <? get_template_part( 'template-parts/modals/be-inspired' ) ?>
</div>
