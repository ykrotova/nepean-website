<?php

global $post;
?>

<?php if ( $image = get_the_post_thumbnail_url( $post, 'medium_large' ) ): ?>
  <div class="loop-item">
    <div class="position-relative overflow-hidden">
      <a href="<?= get_permalink() ?>" class="loop-item__image" style="background-image: url(<?= $image; ?>)"></a>

      <?php if ( $cat = get_last_term( $post->ID, 'category' ) ): ?>
        <span class="loop-item__cat"><?= $cat->name; ?></span>
      <?php endif; ?>
    </div>

    <div class="loop-item__text balance-elements">
      <a href="<?= get_permalink() ?>">
        <div class="balance-elements mb-3">
          <h5 class="loop-item__title"><?= get_the_title() ?></h5>

          <?= wpautop( wp_trim_words( $post->post_content, 20 ) ) ?>
        </div>

        <span class="btn--arrow"></span>
      </a>
    </div>
  </div>
<?php endif;
