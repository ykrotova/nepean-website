<?php

global $post;
?>
<div class="block-post">
  <div class="block-post__image corner--top-left">
    <a href="<?= get_permalink() ?>">
      <? if ( has_post_thumbnail() ) : ?>
        <img class="img-fluid w-100" src="<?= get_the_post_thumbnail_url( $post, 'medium_large' ) ?>" alt="<?= esc_attr( get_the_title() ) ?>">
      <? else: ?>
        <img class="img-fluid w-100" src="<?= get_assets_path( 'images/logo.svg' ) ?>" alt="<?= esc_attr( get_the_title() ) ?>">
      <? endif; ?>
    </a>
  </div>

  <div class="block-post__content">
    <div class="balance-elements p-3 p-xl-4">
      <h6 class="font-weight-bold mb-3">
        <a href="<?= get_permalink() ?>" class="black">
          <? the_title() ?>
        </a>
      </h6>

      <div class="row align-items-center pink mb-3 block-post__meta">
        <div class="col-6">
          <?
          printf( '<p><time class="entry-date published updated" datetime="%1$s">%2$s</time></p>',
            esc_attr( get_the_date( 'c' ) ),
            esc_html( get_the_date( 'M d, Y' ) ) );
          ?>
        </div>

        <div class="col-6 text-right">
          <? if ( ! empty( $term = get_last_term( $post->ID, 'category' ) ) ) : ?>
            <?= $term->name ?>
          <? endif; ?>
        </div>
      </div>

      <? the_excerpt(); ?>
    </div>

    <a href="<?= get_permalink() ?>" class="btn--arrow ml-auto">&nbsp;</a>
  </div>
</div>
