<?php

global $post;
?>
<!-- Modal -->
<div class="modal fade modal--be-inspired" id="modal-be-inspired-<?= $post->ID ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-xxl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><? get_template_part( 'template-parts/close-icon' ) ?></span>
        </button>
      </div>

      <div class="modal-body" data-post-id="<?= $post->ID ?>">
        <p class="py-5 my-5 text-center"><img src="<? the_assets_path( 'images/loading.svg' ); ?>" class="img-fluid my-5" width="80" alt="loading"><span class="sr-only">Loading...</span></p>
      </div>
    </div>
  </div>
</div>
