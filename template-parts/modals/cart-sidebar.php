<div class="modal fade woo-cart__sidebar" id="woo-cart-sidebar" tabindex="-1" role="dialog" aria-labelledby="woo-cart-sidebar-label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><? get_template_part( 'template-parts/close-icon' ) ?></span>
        </button>
      </div>

      <div class="modal-body">
        <div class="woo-cart__wrapper woocommerce">
          <div class="widget_shopping_cart_content"></div>
        </div>
      </div>
    </div>
  </div>
</div>
