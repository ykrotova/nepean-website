<?php

global $post;
?>
<!-- Modal -->
<div class="modal fade" id="modal-fabric-opacity" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-xxl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><? get_template_part( 'template-parts/close-icon' ) ?></span>
        </button>
      </div>

      <div class="modal-body p-0">
        <? echo do_shortcode( '[elementor-template id="25335"]' ); ?>
      </div>
    </div>
  </div>
</div>
