<!-- Modal -->
<div class="modal fade woocommerce" id="ajax-modal-product" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><? get_template_part( 'template-parts/close-icon' ) ?></span>
        </button>
      </div>

      <div class="modal-body">
        <!-- Ajax div to append data -->
        <div id="ajax-modal-product--content"></div>
      </div>
    </div>
  </div>
</div>
