<!-- Modal -->
<div class="modal fade" id="modal-submit-cv" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><? get_template_part( 'template-parts/close-icon' ) ?></span>
        </button>
      </div>

      <div class="modal-body pt-5 px-3 px-lg-5 pb-3 pb-lg-5">
        <h2 class="text-center">Submit your CV</h2>
        <?= do_shortcode( '[contact-form-7 id="25470" title="Submit CV"]' ) ?>
      </div>
    </div>
  </div>
</div>
