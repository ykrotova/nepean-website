<?php

$queried_object = get_queried_object();

if ( ! isset( $queried_object->taxonomy ) ) {
  return;
}

// Pick up taxonomy
$taxonomy = $queried_object->taxonomy;
if ( woo_is_fabric_sample_terms() ) {
  $taxonomy = TERM_FABRIC;
} elseif ( woo_is_spare_parts_terms() ) {
  $taxonomy = TERM_SPARE_PARTS;
}

// List out terms
$terms = get_terms( [
  'taxonomy'   => $taxonomy,
  'parent'     => 0,
  'hide_empty' => true,
] );

// Append "All" category to corresponding taxonomies list
if ( woo_is_fabric_sample_terms() ) {
  $category       = get_term_by( 'slug', CAT_FABRIC, 'product_cat' );
  $category->name = 'All';

  if ( is_array( $terms ) ) {
    array_unshift( $terms, $category );
  }
} elseif ( woo_is_spare_parts_terms() ) {
  $category       = get_term_by( 'slug', CAT_SPARE_PARTS, 'product_cat' );
  $category->name = 'All';

  if ( is_array( $terms ) ) {
    array_unshift( $terms, $category );
  }
}

if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) : ?>
  <div class="block-top-shop">
    <div class="container">
      <div class="row row--main justify-content-center">
        <? foreach ( $terms as $term ) : $featured_icon = get_field( 'featured_icon', $term ); ?>
          <div class="col <?= ( $term->slug == $queried_object->slug ) ? 'active' : '' ?> <?= (!empty($featured_icon)) ? 'col--has-icon' : '' ?>">
            <a href="<?= get_term_link( $term ) ?>" class="d-table">
              <? if ( ! empty( $featured_icon ) ) : ?>
                <div class="d-table-row">
                  <div class="d-table-cell align-middle pt-3">
                    <img alt="<?= esc_attr( $term->name ) ?>" src="<?= $featured_icon['url'] ?>"/>
                  </div>
                </div>
              <? endif; ?>

              <div class="d-table-row">
                <div class="d-table-cell align-middle">
                  <h3><?= $term->name ?></h3>
                </div>
              </div>
            </a>
          </div>
        <? endforeach; ?>
      </div>
    </div>
  </div>

  <div class="container text-center">
    <?php
    /**
     * Hook: woocommerce_archive_description.
     *
     * @hooked woocommerce_taxonomy_archive_description - 10
     * @hooked woocommerce_product_archive_description - 10
     */
    do_action( 'woocommerce_archive_description' );
    ?>
  </div>

  <? if ( woo_has_products_filter_topbar() ) : ?>
    <div class="filter-by mb-3 mb-xl-4">
      <div class="container container--products">
        <div class="filter-by__wrapper align-items-center">
          <h6 class="mb-0 yith-woocommerce-ajax-product-filter">Filter by</h6>
          <?
          ! is_active_sidebar( 'by_filter' ) or dynamic_sidebar( 'by_filter' );
          ?>
        </div>
      </div>
    </div>
  <? endif; ?>
<?php endif;
