<?php

global $product;

/** @var WC_Product $product */
if ( woo_product_is_fabric_samples( $product ) ) { ?>
  <div class="<?= ( isset( $args['wrapper_classes'] ) ) ? esc_attr( $args['wrapper_classes'] ) : esc_attr( CLASSES_PRODUCT_LOOP_SPACING ) ?> balance-elements mb-2 product-attributes--light">
    <? wc_display_product_attributes( $product ); ?>
  </div>
  <?
}
