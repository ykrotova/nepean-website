<?php

global $product;

/** @var WC_Product_Variable $product */
if ( woo_product_is_fabric_samples( $product ) && $product->is_type( 'variable' ) ) : ?>
  <?php
  $variations   = $product->get_available_variations( 'objects' );
  $colour_terms = wp_get_post_terms( $product->get_id(), TERM_COLOUR );

  $shown_colours = [];
  $count         = 0;

  if ( ! empty( $variations ) ) : ?>
    <div class="colors-palette d-flex mb-md-2 <?= esc_attr( CLASSES_PRODUCT_LOOP_SPACING ) ?>">
      <? foreach ( $variations as $index => $variation ) : ?>
        <?
        /** @var WC_Product_Variation $variation */
        if ( $count > 2 ) {
          break;
        }

        $term_name = $variation->get_attribute( TERM_COLOUR );

        if ( in_array( $term_name, $shown_colours ) ) {
          continue;
        }

        $shown_colours[] = $term_name;
        $count ++;
        ?>
        <div class="colors-palette__item">
          <div class="colors-palette__img"><?= $variation->get_image( 'thumbnail' ) ?></div>
          <div class="colors-palette__title"><?= $term_name ?></div>
        </div>
      <? endforeach; ?>

      <? if ( isset( $index ) && ( $index + 1 < count( $variations ) ) ) : $left = count( $colour_terms ) - $count; ?>
        <span class="colors-palette__more d-none d-lg-block">+<?= $left ?> more <?= _n( 'colour', 'colours', $left ) ?></span>
        <span class="colors-palette__more d-lg-none mt-2">+<?= count( $colour_terms ) ?> more <?= _n( 'colour', 'colours', $left ) ?></span>
      <? endif; ?>
    </div>
  <?php endif; ?>
<?php endif;
