<?php

global $product;

echo '<div class="woocommerce-product-gallery woocommerce-product-gallery--with-images images woo-images">';

if ( ! empty( $wistia_videos = get_field( 'wistia_videos' ) ) ) {
  foreach ( $wistia_videos as $video ) {
    $esc_attr = esc_attr( $video['wistia_video_id'] );
    ?>
    <script src="https://fast.wistia.com/embed/medias/<?= $esc_attr ?>.jsonp" async></script>

    <?php /*
    <!-- Responsive block -->
    <div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;">
      <div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;">
        <div class="wistia_embed wistia_async_<?= $esc_attr ?> videoFoam=true" style="height:100%;position:relative;width:100%">
          <div class="wistia_swatch" style="height:100%;left:0;opacity:0;overflow:hidden;position:absolute;top:0;transition:opacity 200ms;width:100%;"><img src="https://fast.wistia.com/embed/medias/<?= $esc_attr ?>/swatch" style="filter:blur(5px);height:100%;object-fit:contain;width:100%;" alt="" aria-hidden="true" onload="this.parentNode.style.opacity=1;"/></div>
        </div>
      </div>
    </div>
    */ ?>

    <!-- Popover -->
    <div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;">
      <div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_<?= $esc_attr ?> popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;position:relative;width:100%">&nbsp;</span></div>
    </div>
    <?php
  }
}

if ( $product->get_image_id() ) {
  echo wc_get_gallery_image_html( $product->get_image_id(), true );
} else {
  echo '<div class="woocommerce-product-gallery__image--placeholder">';
  echo sprintf( '<img src="%s" alt="%s" class="wp-post-image" />', esc_url( wc_placeholder_img_src( 'woocommerce_single' ) ), esc_html__( 'Awaiting product image', 'woocommerce' ) );
  echo '</div>';
}

$attachment_ids = $product->get_gallery_image_ids();
if ( $attachment_ids && $product->get_image_id() ) {
  ?>
  <? foreach ( $attachment_ids as $attachment_id ) : ?>
    <? echo wc_get_gallery_image_html( $attachment_id, true ); ?>
  <? endforeach; ?>
  <?php
}

echo '</div>';
