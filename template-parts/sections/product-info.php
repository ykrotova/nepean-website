<?php

global $product;

$is_normal_product = woo_product_is_normal_product( $product );

/** @var WC_Product $product */
if ( ! woo_product_is_fabric_samples( $product ) ) {
  echo '<div class="read-more mb-4">';
  echo wc_format_content( $product->get_description() );
  echo '</div>';
}

if ( $is_normal_product ) : ?>
  <div class="clearfix mt-4">
    <a href="#" data-toggle="modal" data-target="#modal-free-consultant" class="btn mr-2 mb-3 btn--full-width--mobile">Free consultation</a>
    <a href="/interest-free-finance/" class="btn mb-3 btn--full-width--mobile">Finance options</a>
  </div>
<?php endif;

if ( $is_normal_product ) :
  $tabs = [];

  if ( ! empty( $available_fabric = get_field( 'available_fabric' ) ) ) {
    ob_start();

    if ( ! empty( $available_fabric ) ) : ?>
      <p class="mb-2">Select a fabric to order a sample</p>

      <div class="colors-palette colors-palette--medium d-flex mb-3">
        <? foreach ( $available_fabric as $index => $_post ) : $_product = wc_get_product( $_post->ID ); ?>
          <? /** @var WC_Product $_product */ ?>
          <a href="#" class="colors-palette__item" onclick="Woo.toggleProductAjaxModal(<?= $_post->ID ?>); return false;">
            <div class="colors-palette__img"><?= $_product->get_image( 'thumbnail' ) ?></div>
            <div class="colors-palette__title"><?= $_product->get_name() ?></div>
          </a>
        <? endforeach; ?>
      </div>
    <?php endif; ?>

    <?php /*
    <button class="button alt single_add_to_cart_button">Order free sample</button>
    */ ?>

    <? get_template_part( 'template-parts/explore-link' ) ?>

    <?php
    $tabs['fabric'] = [
      'label'   => 'Available fabric',
      'content' => ob_get_clean(),
    ];
  }

  $technical_information_files = get_field( 'fact_sheet_pdf' );
  $global_catalogue            = get_field( 'global_catalogue', 'options' );

  if (
    ( ! empty( $technical_information_files ) ) ||
    ( ! empty( $global_catalogue ) )
  ) {
    ob_start();
    ?>
    <div class="container-fluid woo-downloads">
      <div class="row align-items-center">
        <?
        $count = 0;
        foreach ( $technical_information_files as $index => $file_item ) : ?>
          <div class="col col-lg-6">
            <a href="<?= $file_item['select_pdf']['url'] ?>" download target="_blank" class="d-flex align-items-center p-1 p-lg-3">
              <img src="<? the_assets_path( 'images/file-pdf.svg' ); ?>" class="img-fluid mr-2" width="25" alt="PDF">

              <span class="mr-3 woo-downloads__title"><?= ! empty( $file_item['add-title'] ) ? esc_html( $file_item['add-title'] ) : 'Fact Sheet ' . ( ++ $count ) ?></span>

              <img src="<? the_assets_path( 'images/download.svg' ); ?>" class="img-fluid ml-auto mr-xl-4" width="16" alt="Download">
            </a>
          </div>

          <? if ( ! empty( 1 == $index % 2 ) && ( $index + 1 < count( $technical_information_files ) ) ) : ?>
            <?= '</div><div class="row align-items-center">' ?>
          <? endif; ?>
        <? endforeach; ?>

        <? if ( ! empty( $global_catalogue ) ) : ?>
          <div class="col col-lg-6">
            <a href="<?= esc_url( $global_catalogue ) ?>" target="_blank" class="d-flex align-items-center p-1 p-lg-3">
              <img src="<? the_assets_path( 'images/file-html.svg' ); ?>" class="img-fluid mr-2" width="25" alt="PDF">

              <span class="mr-3 woo-downloads__title">Wynstan Catalogue</span>

              <img src="<? the_assets_path( 'images/download.svg' ); ?>" class="img-fluid ml-auto mr-xl-4" width="16" alt="Download">
            </a>
          </div>
        <? endif; ?>
      </div>
    </div>
    <?php
    $tabs['tech'] = [
      'label'   => 'Technical information',
      'content' => ob_get_clean(),
    ];
  }

  if ( ! empty( $tabs ) ) : ?>
    <div class="mt-4 mt-xl-5">
      <?= woo_tabs_html( $tabs ); ?>
    </div>
  <? endif; ?>
<? endif;
