<?php

global $product;
?>
<div class="order-fabric">
  <div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>
    <div class="row align-items-center">
      <div class="col-xl-6">
        <? get_template_part( 'template-parts/edit-post-link', null, [ 'style' => 'left:25px;top:10px;' ] ) ?>

        <?
        do_action( 'woocommerce_before_single_product_summary' );
        ?>
      </div>

      <div class="col-xl-6">
        <?
        the_title( '<h3 class="product_title entry-title mb-3">', '</h3>' );
        get_template_part( 'template-parts/sections/product-attributes', null, [ 'wrapper_classes' => '' ] );
        ?>

        <h6 class="variations_title mt-4 mb-3">Select your options</h6>
        <?
        remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
        remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );

        do_action( 'woocommerce_single_product_summary' );
        ?>

        <? if ( isset( $args['show_more_fabric_link'] ) && $args['show_more_fabric_link'] ) : ?>
          <? get_template_part( 'template-parts/explore-link' ) ?>
        <? endif; ?>
      </div>
    </div>
  </div>
</div>
