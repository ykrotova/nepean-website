<?php

global $product;

/** @var WC_Product $product */
if ( woo_product_is_normal_product( $product ) ) : ?>
  <div class="<?= esc_attr( CLASSES_PRODUCT_LOOP_SPACING ) ?> balance-elements mb-5"><?= wpautop( $product->get_short_description() ) ?></div>
<?php endif;
