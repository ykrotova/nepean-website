<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
do_action( 'woocommerce_before_main_content' );

// Archive image
$archive_bg = get_assets_path( 'images/img02.jpg' );
if ( ! empty( $featured_image = get_field( 'featured_image', get_queried_object() ) ) ) {
  $archive_bg = $featured_image['url'];
}

// Sidebar
$has_sidebar = woo_has_products_filter_sidebar();
?>
  <header class="woocommerce-products-header bg-center <?= ! woo_has_archive_banner() ? 'sr-only' : '' ?>" style="background-image: url('<?= $archive_bg ?>');">
    <?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
      <h1 class="woocommerce-products-header__title page-title sr-only"><?php woocommerce_page_title(); ?></h1>
    <?php endif; ?>
  </header>

<?php
/**
 * Hook: woocommerce_before_shop_loop.
 *
 * @hooked woocommerce_output_all_notices - 10
 * @hooked woocommerce_result_count - 20
 * @hooked woocommerce_catalog_ordering - 30
 */
do_action( 'woocommerce_before_shop_loop' );
?>
  <div class="container container--products">
    <? if ( $has_sidebar ) : ?>
    <div class="row">
      <div class="col-lg-3 pr-lg-5">
        <? get_sidebar() ?>
      </div>

      <div class="col-lg-9">
        <? endif; ?>
        <?php

        woocommerce_product_loop_start();

        if ( woocommerce_product_loop() ) {
          if ( wc_get_loop_prop( 'total' ) ) {
            while ( have_posts() ) {
              the_post();

              /**
               * Hook: woocommerce_shop_loop.
               */
              do_action( 'woocommerce_shop_loop' );

              wc_get_template_part( 'content', 'product' );
            }
          }
        } else {
          /**
           * Hook: woocommerce_no_products_found.
           *
           * @hooked wc_no_products_found - 10
           */
          do_action( 'woocommerce_no_products_found' );
        }

        woocommerce_product_loop_end();

        /**
         * Hook: woocommerce_after_shop_loop.
         *
         * @hooked woocommerce_pagination - 10
         */
        do_action( 'woocommerce_after_shop_loop' );

        if ( $has_sidebar ) : ?>
      </div>
    </div>
  <? endif; ?>
  </div>
<?php

/**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action( 'woocommerce_after_main_content' );

/**
 * Hook: woocommerce_sidebar.
 *
 * @hooked woocommerce_get_sidebar - 10
 */
do_action( 'woocommerce_sidebar' );

get_footer( 'shop' );
