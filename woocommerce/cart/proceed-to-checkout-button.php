<?php
/**
 * Proceed to checkout button
 *
 * Contains the markup for the proceed to checkout button on the cart.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/proceed-to-checkout-button.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly.
}
?>

<? if ( is_cart() ) : ?>
  <a href="<?php echo esc_url( wc_get_checkout_url() ); ?>" class="button checkout wc-forward">
    <?php esc_html_e( 'Checkout now', 'woocommerce' ); ?>
  </a>
<? else: ?>
  <div class="row">
    <div class="col-sm-5 mb-3 mb-sm-0">
      <? if ( ! WC()->cart->is_empty() ) : ?>
        <a href="<?= esc_url( wc_get_cart_url() ) ?>" class="edit-cart button checkout btn--outline">View cart</a>
      <? endif; ?>
    </div>

    <div class="col-sm-7">
      <a href="<?php echo esc_url( wc_get_checkout_url() ); ?>" class="button checkout wc-forward">
        <?php esc_html_e( 'Checkout now', 'woocommerce' ); ?>
      </a>
    </div>
  </div>
<? endif;
